const dir = './fragments/';

$("#login").load(dir+'login.html');
$("#help").load(dir+'help.html');
$("#sobre").load(dir+'sobre.html');
$("#configuracoes").load(dir+'configuracoes.html');
$("#loading-bar").load(dir+'loading-bar.html');
$("#loading").load(dir+'loading.html');
$("#uploading").load(dir+'uploading.html');
$("#offline").load(dir+'offline.html');
$("#header").load(dir+'header.html');
$("#menu").load(dir+'menu.html');
$("#conteudo").load(dir+'home.html');
$("#botao_scanner").load(dir+'botao_scanner.html');