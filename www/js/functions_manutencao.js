// MANUTENÇÕES
function mostrarManutencoes(url=null, sem_historico=false){
    getManutencoes(url, (registros, infos=null)=>{
        if(registros === false){
            toast('Ocorreu um erro ao consultar as Manutenções.\nTente novamente mais tarde.', 'erro', 'long')
        }else {
            carregaConteudo(
                './fragments/manutencao/index.html',
                'Manutenções',
                sem_historico === false ? 'manutencoes' : null,
                ()=>{
                    setTimeout(()=> {
                        $.get('./fragments/manutencao/json/manutencao_columns.json', (colunas)=>{
                            colunas = JSON.parse(colunas);
                            $("#tabela_manutencoes").footable({
                                "empty": "<img src='./img/neandertal_sem_internet.svg' width='200px'>",
                                "columns": colunas,
                                "rows": registros,
                                "on": {
                                    'postinit.ft.table': (e, ft)=>{
                                        $(".footable-filtering-search .input-group .input-group-btn .dropdown-toggle").remove();
                                    }
                                }
                            });

                            if(infos !== null) {
                                pagination = renderPagination(infos, 'mostrarManutencoes', [true]);
                                $("#manuts_athenas").append(pagination);
                                $('select').formSelect();
                            }

                            $("#tabela_manutencoes").removeClass('hide');
                            $("#conteudo_manutencao .progress").hide();

                            $('.tooltipped').tooltip();
                        });
                    }, 500);
                }
            );
        }
    });
}

function getManutencoes(url=null, callback=null){
    if(navigator.connection.type === Connection.NONE){
        callback([]);
    }else {
        url = url === null ? "manutencoes/" : url.replace(url_athenas, '');
        let registros = [];
        let settings = ajaxSettings('GET', url);
        $.ajax(settings).done(function (manutencoes, jqXHR) {
            if (manutencoes['count'] > 0) {
                let infos = {
                    "total": manutencoes['count'],
                    "proximo": manutencoes['next'],
                    "anterior": manutencoes['previous']
                };
                $.each(manutencoes['results'], function (index, manutencao) {
                    registros.push({
                        'id': manutencao['id'],
                        'status': manutencao['status_display'],
                        'codigo': manutencao['codigo'],
                        'tipo': manutencao['tipo_display'],
                        'origem': manutencao['origem'],
                        'destino': manutencao['destino'],
                        'lotacao_atual': manutencao['lotacao_atual'],
                        'responsavel': manutencao['responsavel'],
                        'data': manutencao['data'],
                        'prazo': manutencao['prazo'],
                        'descricao': manutencao['descricao'],
                        'substituido_por': manutencao['substituido_por'],
                        'usuario': manutencao['usuario'],
                        'patrimonio': '<b>'+manutencao['patrimonio']['plaqueta']+'</b> - '+manutencao['patrimonio']['especie'],
                        'botao_movimentacao': '<button type="button" ' +
                            'onclick="mostrarMovimentacoesManutencao(' + manutencao['id'] + ');" ' +
                            'class="btn btn-flat btn-small"><i class="material-icons">remove_red_eye</i></button>'
                    });
                    if (manutencoes['results'].length - 1 === index) {
                        if (callback !== null) {
                            callback(registros, infos);
                        } else {
                            return registros;
                        }
                    }
                });
            } else {
                if (callback !== null) {
                    callback(registros);
                } else {
                    return registros;
                }
            }
        }).fail(function (jqXHR, status) {
            return false;
        });
    }
}

function buscarManutencoes() {
    mostrarManutencoes("manutencoes/?codigo="+$("#manuts_athenas_searchfield").val(), true);
}

function mostrarMovimentacoesManutencao(manutencao){
    $("#movimentos_manutencao__title").html('');
    $("#movimentos_manutencao__content").html('<div class="progress"><div class="indeterminate"></div></div>');
    $('#movimentos_manutencao').modal({onCloseEnd: ()=>{remHistorico()}});
    $('#movimentos_manutencao').modal('open');
    addHistorico('modal|movimentos_manutencao');
    getMovimentacoesManutencao(manutencao, (registros, infos=null)=>{
        if(registros === false){
            toast(
                'Ocorreu um erro ao consultar os Movimentos da Manutenção.\nTente novamente mais tarde.',
                'erro',
                'long'
            )
        }else {
            setTimeout(()=> {
                renderMovimentosManutencao(registros, (itens_rendered)=>{
                    $("#movimentos_manutencao__content").fadeOut('fast', ()=>{
                        $("#movimentos_manutencao__content").html(itens_rendered);
                        $("#movimentos_manutencao__content").fadeIn('fast');
                    })
                })
            }, 500);
        }
    });
}

function getMovimentacoesManutencao(manutencao, callback){
    let registros = [];
    let settings = ajaxSettings('GET', "manutencoes_movimentos/?manutencao="+manutencao+"&limit=9999");
    $.ajax(settings).done(function (itens_mov, jqXHR) {
        if(itens_mov['count'] > 0) {
            let infos = {
                "total": itens_mov['count'],
                "proximo": itens_mov['next'],
                "anterior": itens_mov['previous']
            };
            $.each(itens_mov['results'], function (index, item) {
                registros.push({
                    'id': item['id'],
                    'origem': item['origem_display'],
                    'destino': item['destino_display'],
                    'status': item['status_display'],
                    'usuario': item['usuario_display'],
                    'data': item['data'],
                    'descricao': item['descricao'],
                    'responsavel': item['responsavel_display']
                });
                if(itens_mov['results'].length-1 === index){
                    if(callback !== null) { callback(registros, infos); }else{ return registros; }
                }
            });
        }else{
            if(callback !== null){ callback(registros); }else{ return registros; }
        }
    }).fail( function(jqXHR, status){
        return false;
    });
}


function lerPatrimonioManutencao() {
    abreScanner(patrimonio=>mostrarFormNovaManutencao(patrimonio));
}


function mostrarFormNovaManutencao(patrimonio){
    carregaConteudo(
        './fragments/manutencao/nova.html',
        'Manutenção',
        'nova_manutencao',
        ()=>{
            $("#nova_manutencao__patrimonio").val(patrimonio);
            M.updateTextFields();
            $.each([
                'nova_manutencao__destino', 'nova_manutencao__responsavel', 'nova_manutencao__substituto'
            ], (i, campo)=>{
                $("#"+campo).autocomplete({minLength: 3});
                $("#"+campo).on("input change", (e)=>{
                    if(e.target.id === 'nova_manutencao__destino'){
                        url = 'localizacoes/?limit=9999&titulo='+e.target.value
                    }else if(e.target.id === 'nova_manutencao__responsavel'){
                        url = 'pessoas-simplificado/?nome='+e.target.value+'&cpf_cnpj='+e.target.value
                    }else{
                        url = 'patrimonios/?plaqueta='+e.target.value
                    }
                    settings = ajaxSettings('GET', url);
                    $.ajax(settings).done((result, jqXHR)=>{
                        if(result['count'] > 0) {
                            var registros = {};
                            $.each(result['results'], (index, reg)=>{
                                if(e.target.id === 'nova_manutencao__destino'){
                                    registros[reg['id']+': '+reg['titulo']] = null;
                                }else if(e.target.id === 'nova_manutencao__responsavel'){
                                    registros[reg['id']+': '+reg['cpf_cnpj']+' - '+reg['nome']] = null;
                                }else{
                                    registros[reg['plaqueta']+': '+reg['especie']['titulo']] = null;
                                }
                                if(result['results'].length-1 === index){
                                    $("#"+campo).autocomplete("updateData", registros);
                                }
                            });
                        }
                    }).fail((jqXHR, status)=>{
                        toast('Falha ao consultar no sistema Athenas.', 'erro');
                    });
                });
            });
        }
    )
}

function createNovaManutencao() {
    $("#uploading").show();
    makeFormData({
        'patrimonio': $("#nova_manutencao__patrimonio").val(),
        'descricao': $("#nova_manutencao__descricao").val(),
        'substituido_por': $("#nova_manutencao__substituto").val() !== '' ? $("#nova_manutencao__substituto").val().split(': ')[0] : null,
        'destino': $("#nova_manutencao__destino").val().split(': ')[0],
        'responsavel': $("#nova_manutencao__responsavel").val().split(': ')[0]
    }, dataset=>{
        let settings = ajaxSettings('POST', 'manutencoes/', dataset);
        $.ajax(settings).done((request, jqXHR)=>{
            $("#uploading").hide();
            toast('Manutenção '+request['numero']+'/'+request['ano']+' criada com sucesso!', 'sucesso');
            mostrarManutencoes();
        }).fail((data)=>{
            $("#uploading").hide();
            toast('Ocorreu um erro ao enviar os dados ao Athenas.', 'erro', 'long');
            console.log(data);
        });
    });
}
