// MOVIMENTAÇÕES
function mostrarMovimentacoes(url=null, sem_historico=false){
    getMovimentacoes(url, (registros, infos=null)=>{
        if(registros === false){
            toast('Ocorreu um erro ao consultar as Movimentações.\nTente novamente mais tarde.', 'erro', 'long')
        }else {
            carregaConteudo(
                './fragments/movimentacao/index.html',
                'Movimentações',
                sem_historico === false ? 'movimentacoes' : null,
                ()=>{
                    setTimeout(()=> {
                        // ATHENAS
                        $.get('./fragments/movimentacao/json/movimentacao_columns.json', (colunas)=>{
                            colunas = JSON.parse(colunas);
                            $("#tabela_movimentacoes").footable({
                                "empty": "<img src='./img/neandertal_sem_internet.svg' width='200px'>",
                                "columns": colunas,
                                "rows": registros,
                                "on": {
                                    'postinit.ft.table': (e, ft)=>{
                                        $(".footable-filtering-search .input-group .input-group-btn .dropdown-toggle").remove();
                                    }
                                }
                            });

                            if(infos !== null) {
                                pagination = renderPagination(infos, 'mostrarMovimentacoes', [true]);
                                $("#movims_athenas").append(pagination);
                                $('select').formSelect();
                            }

                            $("#tabela_movimentacoes").removeClass('hide');
                            $("#conteudo_movimentacao .progress").hide();

                            // APLICATIVO
                            getLotesMovimentacao((registros)=> {
                                renderLotesMovimentacao(registros, (html) => {
                                    $("#movims_aplicativo__lista").html(html);
                                    $('.collapsible').collapsible();
                                })
                            });

                            $('.tooltipped').tooltip();
                        });
                    }, 500);
                }
            );
        }
    });
}

function getMovimentacoes(url=null, callback=null){
    if(navigator.connection.type === Connection.NONE){
        callback([]);
    }else {
        url = url === null ? "movimentacoes/" : url.replace(url_athenas, '');
        let registros = [];
        let settings = ajaxSettings('GET', url);
        $.ajax(settings).done(function (movimentacoes, jqXHR) {
            if (movimentacoes['count'] > 0) {
                let infos = {
                    "total": movimentacoes['count'],
                    "proximo": movimentacoes['next'],
                    "anterior": movimentacoes['previous']
                };
                $.each(movimentacoes['results'], function (index, movimento) {
                    let icone_app = movimento['proveniente_aplicativo'] === true ? '<i class="material-icons" style="font-size: 12px;">phone_iphone</i>' : '';
                    registros.push({
                        'id': movimento['id'],
                        'status': movimento['status_display'],
                        'recebido_por': movimento['recebido_por_display'],
                        'validado_por': movimento['validado_por_display'],
                        'origem': movimento['origem_display'],
                        'destino': movimento['destino_display'],
                        'responsavel_origem': movimento['responsavel_origem_display'],
                        'responsavel_destino': movimento['responsavel_destino_display'],
                        'co_responsavel': movimento['co_responsavel_display'],
                        'numero': movimento['numero_cache'] + icone_app,
                        'movimentado': movimento['movimentado'],
                        'recebido': movimento['recebido'],
                        'validado': movimento['validado'],
                        'proveniente_inventario': movimento['proveniente_inventario'] === false ? 'Não' : 'Sim',
                        'botao_itens': '<button type="button" ' +
                            'onclick="mostrarItensMovimentacao(' + movimento['id'] + ', \'' + movimento['numero_cache'] + '\');" ' +
                            'class="btn btn-flat btn-small"><i class="material-icons">remove_red_eye</i></button>'
                    });
                    if (movimentacoes['results'].length - 1 === index) {
                        if (callback !== null) {
                            callback(registros, infos);
                        } else {
                            return registros;
                        }
                    }
                });
            } else {
                if (callback !== null) {
                    callback(registros);
                } else {
                    return registros;
                }
            }
        }).fail(function (jqXHR, status) {
            return false;
        });
    }
}

function buscarMovimentacoes() {
    mostrarMovimentacoes("movimentacoes/?numero_cache="+$("#movim_athenas_searchfield").val(), true);
}


// ITENS DE MOVIMENTAÇÃO
function mostrarItensMovimentacao(movimento, numero_movimento){
    $("#itens_movimentacao__title").html(numero_movimento);
    $("#itens_movimentacao__content").html('<div class="progress"><div class="indeterminate"></div></div>');
    $('#itens_movimentacao').modal({onCloseEnd: ()=>{remHistorico()}});
    $('#itens_movimentacao').modal('open');
    addHistorico('modal|itens_movimentacao');
    getItensMovimentacao(movimento, (registros, infos=null)=>{
        if(registros === false){
            toast(
                'Ocorreu um erro ao consultar os Itens da Movimentação.\nTente novamente mais tarde.',
                'erro',
                'long'
            )
        }else {
            setTimeout(()=> {
                renderItensMovimentacao(registros, (itens_rendered)=>{
                    $("#itens_movimentacao__content").fadeOut('fast', ()=>{
                        $("#itens_movimentacao__content").html(itens_rendered);
                        $("#itens_movimentacao__content").fadeIn('fast');
                    })
                })
            }, 500);
        }
    });
}

function getItensMovimentacao(movimento, callback){
    let registros = [];
    let settings = ajaxSettings('GET', "movimentacoes-itens/?movimento="+movimento+"&limit=9999");
    $.ajax(settings).done(function (itens_mov, jqXHR) {
        if(itens_mov['count'] > 0) {
            let infos = {
                "total": itens_mov['count'],
                "proximo": itens_mov['next'],
                "anterior": itens_mov['previous']
            };
            $.each(itens_mov['results'], function (index, item) {
                registros.push({
                    'id': item['id'],
                    'plaqueta': item['plaqueta'],
                    'especie': item['especie'],
                    'descricao': item['descricao'],
                    'localizacao': item['localizacao']
                });
                if(itens_mov['results'].length-1 === index){
                    if(callback !== null) { callback(registros, infos); }else{ return registros; }
                }
            });
        }else{
            if(callback !== null){ callback(registros); }else{ return registros; }
        }
    }).fail( function(jqXHR, status){
        return false;
    });
}


// LOTES DE MOVIMENTAÇÃO
function getLotesMovimentacao(callback){
    dbSelect(
        "SELECT * FROM patrimonio_movimento ORDER BY id DESC",
        (movimentos)=>{ callback(movimentos); }
    );
}

function mostrarFormNovoLoteMovimentacao(){
    carregaConteudo(
        './fragments/movimentacao/novo_lote.html',
        'Movimentações',
        'movimentacao_novo_lote',
        ()=>{
            $.each(['novo_lote__responsavel', 'novo_lote__co_responsavel'], (i, campo)=>{
                $("#"+campo).autocomplete({minLength: 3});
                $("#"+campo).on("input change", (e)=>{
                    settings = ajaxSettings(
                        'GET',
                        'servidores-simplificado/?pessoa_fisica__nome='+e.target.value
                    );
                    $.ajax(settings).done((servidores, jqXHR)=>{
                        if(servidores['count'] > 0) {
                            var registros = {};
                            $.each(servidores['results'], (index, servidor)=>{
                                registros[servidor['id']+': '+servidor['pessoa_fisica']['nome']] = null;
                                if(servidores['results'].length-1 === index){
                                    $("#"+campo).autocomplete("updateData", registros);
                                }
                            });
                        }
                    }).fail((jqXHR, status)=>{
                        toast('Falha ao consultar os servidores no sistema Athenas.', 'erro');
                    });
                });
                $("#novo_lote__destino").autocomplete({minLength: 3});
                $("#novo_lote__destino").on("input change", (e)=>{
                    settings = ajaxSettings(
                        'GET',
                        'localizacoes/?limit=9999&titulo='+e.target.value
                    );
                    $.ajax(settings).done((localizacoes, jqXHR)=>{
                        if(localizacoes['count'] > 0) {
                            var registros = {};
                            $.each(localizacoes['results'], (index, localizacao)=>{
                                registros[localizacao['id']+': '+localizacao['titulo']] = null;
                                if(localizacoes['results'].length-1 === index){
                                    $("#novo_lote__destino").autocomplete("updateData", registros);
                                }
                            });
                        }
                    }).fail((jqXHR, status)=>{
                        toast('Falha ao consultar as Localidades no sistema Athenas.', 'erro');
                    });
                });
            });
        }
    )
}

function createNovoLoteMovimentacao() {
    let destino = $("#novo_lote__destino").val();
    let responsavel = $("#novo_lote__responsavel").val();
    let co_responsavel = $("#novo_lote__co_responsavel").val();

    let campos_nomes = ['Destino', 'Responsável', 'Co-Responsável'];
    $.each([destino, responsavel, co_responsavel], (i, campo)=>{
        if(!campo.includes(': ') && (i !== 2 || (i === 2 && campo.length > 0))){
            toast(
                'O campo ' + campos_nomes[i] + ' foi preenchido com um valor inválido!',
                'erro',
                'long'
            );
            return false;
        }else if(i === 2){
            let hoje = new Date();
            dbInsert(
                'patrimonio_movimento',
                ['data', 'destino', 'responsavel', 'co_responsavel'],
                [dataAmigavel(hoje, 'aaaa-mm-dd'), destino, responsavel, co_responsavel],
                ()=>{toast('Salvo com sucesso!', 'sucesso', 'fast');},
                ()=>{toast('Erro ao salvar o registro!', 'erro');}
                )
        }
    });
}

function mostrarItensLoteMovimentacao(lote, destino){
    $("#"+destino).html('<div class="progress"><div class="indeterminate"></div></div>');
    getItensLoteMovimentacao(lote, (itens)=>{
        renderItensLoteMovimentacao(itens, lote, (html)=>{
            setTimeout(()=>{
                $("#"+destino).html(html);
            }, 600);
        });
    });
}

function getItensLoteMovimentacao(lote, callback){
    dbSelect(
        "SELECT * FROM patrimonio_movimento_item WHERE movimento = "+lote+" ORDER BY id",
        (itens)=>{
            if(itens === null) return null;
            var registros = [];
            var quant_rets = 0;
            $.each(itens, (i, item)=>{
                consultaPatrimonio(item['plaqueta'], (patrimonio, itm=item)=>{
                    quant_rets += 1;
                    patrimonio[0]['id_item_lote'] = itm['id'];
                    registros.push(patrimonio[0]);
                    if(quant_rets === itens.length) {
                        callback(registros);
                    }
                });
            });
        }
    )
}

function addItemLoteMovimentacao(item, lote){
    consultaPatrimonio(item, (patrimonio)=>{
        dbInsert(
            'patrimonio_movimento_item',
            ['movimento', 'patrimonio', 'plaqueta'],
            [lote, patrimonio[0]['id'], patrimonio[0]['plaqueta']],
            ()=>{
                toast('Patrimônio adicionado com sucesso!', 'sucesso');
                mostrarDetalhesLoteMovimentacao(lote);
            },
            ()=>{
                toast('Não foi possível adicionar o Patrimônio!', 'erro');
            }
        )
    })
}

function mostrarDetalhesLoteMovimentacao(lote) {
    dbSelect('SELECT * FROM patrimonio_movimento WHERE id = '+lote, (registro)=>{
        registro = registro[0];
        carregaConteudo(
            './fragments/movimentacao/detalhes_lote.html',
            'Movimentação',
            'detalhes_lote',
            ()=>{
                if(registro['data_sincronizacao'] === null) {
                    $("#detalhes_lote__sincronizar").attr('href', 'javascript: enviarLoteMovimentacao(' + lote + ')');
                    $('#detalhes_lote__novo_item').attr(
                        'href',
                        'javascript: abreScanner((r)=>{addItemLoteMovimentacao(r, ' + registro['id'] + ');}, formDigitarPlaqueta((r)=>{addItemLoteMovimentacao(r, ' + registro['id'] + ');}));'
                    );
                    $("#detalhes_lote__deletar_lote").attr(
                        'href',
                        'javascript: confirmar_exclusao("patrimonio_movimento", ' + lote + ', "mostrarMovimentacoes");'
                    );
                }else{
                    $("#detalhes_lote__botoes").html('<i>Enviado em '+dataAmigavel(registro['data_sincronizacao'], 'dd/mm/aaaa', false)+'<br>Criados os movimentos: '+registro['movimentos_criados']+'</i>');
                }
                $('#detalhes_lote__destino').html(registro['destino'].split(': ')[1]);
                $('#detalhes_lote__responsavel').html(registro['responsavel'].split(': ')[1]);
                if(registro['co_responsavel'] !== ''){
                    $('#detalhes_lote__co_responsavel').html(registro['co_responsavel'].split(': ')[1]);
                }
                mostrarItensLoteMovimentacao(lote, 'detalhes_lote__itens');
            }
        )
    });
}

function enviarLoteMovimentacao(lote){
    $("#uploading").show();
    dbSelect('SELECT * FROM patrimonio_movimento WHERE id = '+lote, (lote)=>{
        lote = lote[0];
        dbSelect('SELECT * FROM patrimonio_movimento_item WHERE movimento = '+lote['id'], (itens)=>{
            makeFormData({
                'data': dataAmigavel(lote['data'], 'aaaa-mm-dd'),
                'destino': lote['destino'].split(': ')[0],
                'responsavel_destino': lote['responsavel'].split(': ')[0],
                'co_responsavel': lote['co_responsavel'] !== '' ? lote['co_responsavel'].split(': ')[0] : null,
                'proveniente_aplicativo': true,
                'itens': JSON.stringify(itens)
            }, dataset=>{
                let settings = ajaxSettings('POST', 'movimentacoes/', dataset);
                $.ajax(settings).done((request, jqXHR)=>{
                    let sql = "UPDATE patrimonio_movimento SET data_sincronizacao = '"+dataAmigavel(hoje, 'aaaa-mm-dd', false) +
                        "', movimentos_criados = '"+request+"' WHERE id = "+lote['id'];
                    console.log(sql);
                    db.executeSql(sql, [], ()=>{
                        $("#uploading").hide();
                        mostrarMovimentacoes();
                    });
                }).fail((data)=>{
                    $("#uploading").hide();
                    toast('Ocorreu um erro ao enviar os dados ao Athenas.', 'erro', 'long')
                    console.log(data);
                });
            });
        });
    });
}
