function toast(mensagem, tipo='padrao', duracao='short', posicao="bottom"){
    color = "#000000";
    if(tipo === 'sucesso'){
        bg = '#43A047';
    }else if(tipo === 'erro'){
        bg = '#E53935';
    }else if(tipo === 'alerta'){
        bg = '#FFB300';
    }else{
        bg = '#000000';
        color = '#FFFFFF';
    }
    window.plugins.toast.showWithOptions(
        {
            message: mensagem,
            duration: duracao,
            position: posicao,
            addPixelsY: -40,  // added a negative value to move it up a bit (default 0)
            styling: {
                opacity: 0.85, // 0.0 (transparent) to 1.0 (opaque). Default 0.8
                backgroundColor: bg,
                textColor: color, // Ditto. Default #FFFFFF
                // textSize: 20.5, // Default is approx. 13.
                cornerRadius: 16, // minimum is 0 (square). iOS default 20, Android default 100
                horizontalPadding: 20, // iOS default 16, Android default 50
                verticalPadding: 16 // iOS default 12, Android default 30
            }
        }
    );
}

function dataAmigavel(data, formato='dd/mm/aaaa', incr=true){
    if(data === '' || data === null){
        return ''
    }else {
        if (Object.prototype.toString.call(data) !== "[object Date]") {
            data = new Date(data);
        }
        var dd = data.getDate();
        if(incr === true) {
            dd += 1;
        }
        var mm = data.getMonth() + 1;
        var aaaa = data.getFullYear();
        if (dd < 10) dd = '0' + dd;
        if (mm < 10) mm = '0' + mm;
        if (formato == 'dd/mm/aaaa') {
            return dd + '/' + mm + '/' + aaaa;
        } else if (formato == 'aaaa-mm-dd') {
            return aaaa + "-" + mm + "-" + dd;
        }
    }
}

function dataHoraAmigavel(data, formato='dd/mm/aaaa', incr=true){
    dia = dataAmigavel(data, formato, incr);
    let hora = data.getHours();
    let minuto = data.getMinutes();
    let segundo = data.getSeconds();
    if(hora.length < 2) hora = '0'+hora;
    if(minuto.length < 2) minuto = '0'+minuto;
    if(segundo.length < 2) segundo = '0'+segundo;
    return dia+' '+hora+':'+minuto+':'+segundo;
}

function monetario(numero) {
    return 'R$' + Number(numero).toLocaleString(
        'pt-BR',
        {
                    minimumFractionDigits: 2,
                    maximumFractionDigits: 2
                }
    );
}

function dbSelect(sql, callback){
    db.executeSql(sql, [], (result)=>{
        if(result.rows.length === 0) return null;
        var registros = [];
        $.each(new Array(result.rows.length), (i)=>{
            registros.push(result.rows.item(i));
            if(i === result.rows.length-1){
                callback(registros);
            }
        });
    });
}

function dbInsert(tabela, fields, values, onSuccess=null, onFail=null){
    db.executeSql(
        "INSERT INTO "+tabela+" ("+fields.join(', ')+") VALUES ('"+values.join("', '")+"')", [], (insercao)=>{
            if(insercao.rowsAffected === 1){
                if(onSuccess !== null){
                    onSuccess();
                }else{
                    return true;
                }
            }else{
                if(onFail !== null){
                    onFail();
                }else{
                    return false;
                }
            }
        }
    )
}

function consultaPatrimonio(plaqueta, callback) {
    if(navigator.connection.type === Connection.NONE){
        let retPatrimonio = dbSelect(
            "SELECT * FROM patrimonio_patrimonio WHERE plaqueta LIKE '%"+plaqueta+"' ORDER BY id DESC LIMIT 1",
            (r)=>{
                var registros = [];
                $.each(r, (i, pat)=>{
                    registros.push({
                        'id': pat['id'],
                        'especie': {'titulo': pat['descricao']},
                        'plaqueta': pat['plaqueta']
                    });
                    if(i === r.length-1){
                        callback(registros);
                    }
                });
            }
        );
        if(retPatrimonio === null) toast('Não é possível consultar o patrimônio!', 'erro', 'long');
    }else{
        let url = 'patrimonios/?plaqueta='+plaqueta;
        let settings = ajaxSettings('GET', url);
        $.ajax(settings).done((patrimonio, jqXHR)=>{
            if(patrimonio['count'] > 0){
                callback(patrimonio['results']);
            }
        })
    }
}

function makeFormData(objeto, callback) {
    let formdata = new FormData();
    let quant = Object.keys(objeto).length-1;
    for (let [i, key] of Object.keys(objeto).entries()) {
        formdata.append(key, objeto[key]);
        if(i === quant) callback(formdata)
    }
}
