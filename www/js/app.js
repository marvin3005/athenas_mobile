var db;
var url_athenas = 'https://athenas.defensoria.to.def.br/api/';
var dat;
var usuario;
var senha;
var hoje = new Date();
var historico_nav = [];
var autorizacao_global = null;
var inventario_global = null;
var modal_item_scaneado;
var camera_aberta = false;
var startTime = [hoje.getHours(), hoje.getMinutes(), hoje.getSeconds()];
var leitura;
var inventarios_atualizados = [];
var versao_app;
var token_aut = window.localStorage.getItem("token_aut");
var user_id = window.localStorage.getItem("user_id");
var atualiza_inventariados = window.localStorage.getItem("atualiza_inventariados") === 'true';
var remover_leituras = window.localStorage.getItem("remover_leituras") === 'true';
var base_offline = window.localStorage.getItem("base_offline") === 'true';
var permanecer_autenticado = window.localStorage.getItem('permanecer_autenticado') === 'true';

document.addEventListener('offline', function(evt){onOffline()}, false);
document.addEventListener('online', function(evt){onOnline()}, false);
// document.addEventListener('long-press', function(evt){long_press(evt);});

// var viewportScale = 1 / window.devicePixelRatio;
// $("#viewport").attr(
//     "content",
//     "user-scalable=no, initial-scale="+viewportScale+", minimum-scale=0.2, maximum-scale=1, width=device-width"
// );
// GET DPI:
// dpi_x = document.getElementById('conteudo').offsetWidth;
// https://www.phonerated.com/specs-samsung_galaxy_a8_plus_2018
// https://material.io/resources/devices/

// REMOVE OS CSS DE ACORDO COM A RESOLUÇÃO DPI DO DISPOSITIVO
if(window.devicePixelRatio < 3){
    $('#index__xxhdpi').remove();
    $('#element_styles__xxhdpi').remove();
}else{
    $('#index__xhdpi').remove();
    $('#element_styles__xhdpi').remove();
}

$(document).on("change", '.select_pagination', function(e){
    eval(e.target.value);
});

function get(item){
    return window.localStorage.getItem(item);
}

function set(item, valor){
    window.localStorage.setItem(item, valor);
}

function getDateTimeNow(callback=null){
    if(navigator.connection.type === Connection.NONE){
        if(callback){callback(new Date());}else{return (new Date());}
    }else {
        let settings = {
            async: true,
            crossDomain: true,
            url: 'http://worldtimeapi.org/api/timezone/America/Araguaina',
            type: 'GET',
            beforeSend: xhr => xhr.setRequestHeader("Authorization", "Token" + token_aut),
            processData: false,
            contentType: false
        };
        $.ajax(settings).done((request)=>{
            console.log(request.datetime);
            [dia, hora] = request.datetime.split('T');
            [ano, mes, dia] = dia.split('-');
            [hora, minuto, segundo] = hora.split('.')[0].split(':');
            console.log(ano, mes, dia, hora, minuto, segundo);
            dt = new Date(ano, mes - 1, dia, hora, minuto, segundo)
            if(callback){
                callback(dt);
            }else {
                return dt;
            }
        });
    }
}

function ajaxSettings(tipo, url, dataset=null) {
    let settings = {
        async: true,
        crossDomain: true,
        url: url_athenas + url,
        type: tipo.toUpperCase(),
        beforeSend: xhr=>xhr.setRequestHeader("Authorization", "Token " + token_aut),
        processData: false,
        contentType: false
    };
    if(dataset !== null){
        settings.data = dataset;
    }
    return settings;
}

function start(){
    getDateTimeNow((resp)=>{hoje=resp;});

    console.log('INICIOU SCRIPTS');
    // $(".overall").hide().css('visibility', 'visible');
    $("#login").height(window.screen.height * window.devicePixelRatio+'px');

    //INICIA O BANCO DE DADOS
    db = window.sqlitePlugin.openDatabase({name: "athenas", location:'default'});
    dat = new banco;

    if(get('permanecer_autenticado')){
        $("#mobile_sidenav_user a .name").html(usuario);
    }

    cordova.getAppVersion.getVersionNumber().then(function (version) {
        versao_app = version;
        $('#versao_app').append(version);
        $('#versao_app_sobre').html(version);
        $('#build_date_sobre').html(BuildInfo.buildDate);

        if(get('versao_scripts') === "undefined"){
            console.log('Versão Scripts nulo, será setado para ' + version_scripts['versions'][0]);
            set('versao_scripts', version_scripts['versions'][0]);
        }

        checkAppUpdate();

        //VERIFICA SE É A PRIMEIRA EXECUÇÃO DO APLICATIVO
        // window.localStorage.setItem("num_execucoes", 0);
        firstrun();
    });

    $("#header").hide();

    //POPULA DADOS
    if(navigator.connection.type === Connection.NONE){
        $("#offline_tarja").show();
        toast("Não há conexão com a internet.\nNão será possível obter novas informações do sistema Athenas.");
    }else{
        $("#offline_tarja").hide();
    }

    $("#botao_scanner").hide();
    $('.sidenav').sidenav({closeOnClick: true});
    $('.collapsible').collapsible();
    $('.modal').modal();
    $('.dropdown-trigger').dropdown();
    $('.fixed-action-btn').floatingActionButton();

    $('#confirm').modal({onCloseEnd: ()=>{remHistorico()}});

    setTimeout(function () {
        $("#splash").fadeOut();
    }, 2000);

    setTimeout(function () {
        checkAppUpdate();
    }, 300000);

    if(get('permanecer_autenticado') !== 'true' || get('token_aut') === 'undefined' || get('user_id') === 'undefined'){
        $("#login").show();
    }else{
        if(get('token_aut') !== 'undefined' && get('user_id') !== 'undefined') {
            // getAutorizacoes();
            // montarHome();
            $("#login").hide();
        }else{
            $("#login").show();
        }
    }
}

function checkAppUpdate() {
    var updateUrl = "https://athenas.defensoria.to.def.br/static/patrimonio/aplicativo/version.xml";
    window.AppUpdate.checkAppUpdate(
        onSuccess,
        onFail,
        updateUrl
    );

    function onFail() {
        console.log('fail', JSON.stringify(arguments), arguments);
    }
    function onSuccess() {
        console.log('success', JSON.stringify(arguments), arguments);
    }
}

function onOffline(){
    $("#offline").show();
    $("#botao_sync a").addClass('disabled');
    $("#btn_rebuild_itens").attr('disabled', 'disabled');
    $("#btn_enviar_leituras").attr('disabled', 'disabled');
    $("#offline_tarja").show();
}

function onOnline(){
    $("#offline").hide();
    $("#botao_sync a").removeClass('disabled');
    $("#btn_rebuild_itens").removeAttr('disabled');
    $("#btn_enviar_leituras").removeAttr('disabled');
    $("#offline_tarja").hide();
    if(Math.floor(((Math.abs(new Date() - hoje))/1000)/60) > 1){
        toast('Conexão com a internet restabelecida');
    }
}

function firstrun(){
    var i = window.localStorage.getItem('num_execucoes') === null ? 0 : parseFloat(get("num_execucoes"));
    if(i < 1 || typeof i === undefined || i === "NaN" || i == null){
        console.log('primeira execução!');
        set("num_execucoes", 1);
        set("versao_scripts", versao_app);
        dat.criaTabelas();

        atualiza_inventariados = true;
        set("atualiza_inventariados", 'true');
        $("#atualiza_inventariados").attr('checked', 'checked');

        remover_leituras = true;
        set("remover_leituras", 'true');
        $("#remover_leituras").attr('checked', 'checked');

        base_offline = false;
        set("base_offline", 'false');
        $("#base_offline").removeAttr('checked');

        permanecer_autenticado = false;
        set("permanecer_autenticado", 'false');
        $("#permanecer_autenticado").removeAttr('checked');
    }else{
        i = parseFloat(i) + parseFloat(1);
        set("num_execucoes", i);

        console.log(get('versao_scripts'));

        if(get("versao_scripts") !== versao_app){
            console.log('versão scripts diferente da versão do app: ' + get('versao_scripts') + ' - ' + versao_app);
            scripts = [];
            for(var v = version_scripts['versions'].indexOf(get("versao_scripts")); v < version_scripts['versions'].length; v++){
                ver = version_scripts['versions'][v];
                if(version_scripts['scripts'][ver] !== null){
                    console.log(ver + ' adicionado para ser executado');
                    scripts.push(version_scripts['scripts'][ver]);
                }
            }
            scripts.forEach(function(script, index, array){
                db.executeSql(script, [], function(retornoScript){
                    // toast('SUCESSO AO EXECUTAR O SCRIPT '+index);
                    console.log(retornoScript);
                }, function(error){
                    // toast('ERRO AO EXECUTAR O SCRIPT '+index, 'erro', 'long');
                    console.log(error.message);
                });
            });
            set("versao_scripts", versao_app);
        }
    }

    if(atualiza_inventariados === true || atualiza_inventariados === undefined){
        atualiza_inventariados = true;
        $("#atualiza_inventariados").attr('checked', 'checked');
    }else{
        atualiza_inventariados = false;
        set("atualiza_inventariados", 'false');
        $("#atualiza_inventariados").removeAttr('checked');
    }
    if(remover_leituras === true || remover_leituras === undefined){
        remover_leituras = true;
        $("#remover_leituras").attr('checked', 'checked');
    }else{
        remover_leituras = false;
        set("remover_leituras", 'false');
        $("#remover_leituras").removeAttr('checked');
    }
    if(base_offline === true){
        base_offline = true;
        $("#base_offline").attr('checked', 'checked');
    }else{
        base_offline = false;
        set("base_offline", 'false');
        $("#base_offline").removeAttr('checked');
    }
    if(permanecer_autenticado === true){
        permanecer_autenticado = true;
        $("#permanecer_autenticado").attr('checked', 'checked');
    }else{
        permanecer_autenticado = false;
        set("permanecer_autenticado", 'false');
        $("#permanecer_autenticado").removeAttr('checked');
    }
}

function goHome() {
    if(historico_nav[0] === 'help') $("#help").slideUp('fast');
    if(historico_nav[0] === 'sobre') $("#sobre").slideUp('fast');
    historico_nav = [];
    carregaConteudo('./fragments/home.html', '', '');
}

function backButton(){
    if(camera_aberta === false){
        if(historico_nav[historico_nav.length-1] === 'confirm'){
            $("#confirm").fadeOut('fast');
            remHistorico();
        }else if(historico_nav.length === 0){
            confirmar("Deseja fechar o aplicativo?", 'navigator.app.exitApp();');
        }else if(historico_nav.length === 1){
            goHome();
        }else{
            let n = historico_nav[historico_nav.length-1];
            if(n.includes('modal|')){
                modal = n.split('|')[1];
                $("#"+modal).modal('close');
            }else if(n === 'help'){
                help();
            }else if(n === 'sobre'){
                sobre();
            }else{
                n = historico_nav[historico_nav.length-2];
                if(n === 'autorizacoes'){
                    getAutorizacoes();
                    remHistorico();
                }else if(n === 'inventarios'){
                    getInventarios(autorizacao_global);
                    remHistorico();
                }else if(n === 'itens'){
                    getItens(inventario_global, true, false);
                    remHistorico();
                }else if(n === 'movimentacoes'){
                    mostrarMovimentacoes();
                    remHistorico();
                }else if(n === 'movimentacao_novo_lote'){
                    mostrarFormNovoLoteMovimentacao();
                    remHistorico();
                }else if(n === 'manutencoes'){
                    mostrarManutencoes();
                    remHistorico();
                }else if(n === 'nova_manutencao'){
                    mostrarFormNovaManutencao();
                    remHistorico();
                }
            }
        }
    }
}

function carregaConteudo(conteudo, nome_modulo, nome_historico, dataset=null){
    let header = $('#header');
    let div_cont = $('#conteudo');

    if(conteudo !== './fragments/home.html'){
        div_cont.css('padding-top', '56px');
        header.show();
    }else{
        div_cont.css('padding-top', '0');
        header.hide();
    }

    div_cont.slideToggle('fast');
    if(conteudo.indexOf('.html') !== -1) {
       div_cont.load(conteudo, function () {
           if(nome_historico !== null) addHistorico(nome_historico);
           if(dataset !== null){
               if(typeof dataset === 'object') {
                   for (var [elemento, valor] of Object.entries(dataset)) {
                       $(elemento).html(valor);
                   }
               }else{
                   dataset();
               }
           }
           carregaComplementos();
           div_cont.slideToggle('fast');
        });
    }else{
       div_cont.html(conteudo);
       if(nome_historico !== null) addHistorico(nome_historico);
       div_cont.slideToggle('fast');
    }

    $("#module_name").html(nome_modulo);
}

function addHistorico(nav){
    if(historico_nav[historico_nav.length-1] !== nav && nav !== '') {
        historico_nav.push(nav);
    }
}

function remHistorico(){
    console.log('removeu histórico: '+historico_nav[historico_nav.length-1]);
    historico_nav.pop();
    if(historico_nav.length === 0) $("#header").hide();
}

function carregaComplementos() {
    $('.tabs').tabs();
}

function salvarConfiguracoes(){
    atualiza_inventariados = $('#atualiza_inventariados').prop('checked');
    set("atualiza_inventariados", atualiza_inventariados);
    remover_leituras = $('#remover_leituras').prop('checked');
    set("remover_leituras", remover_leituras);
    if(base_offline === true && $("#base_offline").prop('checked') === false){
        sql = 'DELETE FROM patrimonio_patrimonio';
        db.executeSql(sql, [], function (result) {
            console.log(result);
        });
    }
    base_offline = $('#base_offline').prop('checked');
    set("base_offline", base_offline);
    permanecer_autenticado = $('#permanecer_autenticado').prop('checked');
    set('permanecer_autenticado', permanecer_autenticado);
    if(permanecer_autenticado === true){
        set('usuario', get('usuario'));
        set('token_aut', token_aut);
        set('user_id', user_id);
    }

    toast('Configurações atualizadas com sucesso!');

    $("#configuracoes").hide();
}

// function long_press(event){
//     console.log(event);
//     var t = event.target;
//     while(t.tagName !== "BODY"){
//         console.log(t);
//         if(t.hasAttribute('data-lp')){
//             console.log('tem lp!!');
//             var tipo = t.getAttribute('data-lp-tipo');
//             var valor = t.getAttribute('data-lp-valor');
//             if(t.getAttribute('data-lp') === 'delete' && (tipo !== null && valor !== null)){
//                 console.log('lp é delete!');
//                 console.log(tipo);
//                 console.log(valor);
//                 abre_opcoes(tipo, valor);
//                 break;
//             }
//         }else{
//             t = t.parentElement;
//         }
//     }
// }

function abre_opcoes(tipo, valor){
    if(tipo === 'patrimonio_inventarios_leituras'){
        db.executeSql("SELECT * FROM patrimonio_inventarios_leituras L JOIN patrimonio_inventarios_itens I ON I.plaqueta = L.plaqueta WHERE L.id = "+valor, [],
            function(leitura){
                leitura = leitura.rows.item(0);
                ret = "<div class='col s12'>" +
                    "<h5>Leitura</h5><hr>" +
                    "</div>" +
                    "<div class='row left-align'>" +
                    "<div class='col s4 right-align'>Plaqueta:</div>" +
                    "<div class='col s8'>"+leitura.plaqueta+"</div>" +
                    "<div class='col s4 right-align'>Descrição:</div>" +
                    "<div class='col s8'>"+leitura.descricao+"</div>" +
                    "<div class='col s4 right-align'>Leitura:</div>" +
                    "<div class='col s8'>"+dataAmigavel(leitura.data)+"</div>" +
                    "<div class='col s4 right-align'>Subst. Plaqueta:</div>" +
                    "<div class='col s8'>"+(leitura.digitado === 't' ? 'Sim' : 'Não')+"</div>" +
                    "<div class='col s4 right-align'>Enviado:</div>" +
                    "<div class='col s8'>"+(leitura.lancado === 't' ? dataAmigavel(leitura.data_lancamento) : 'Não')+"</div>";
                if(leitura.erro_lancamento !== null){
                    ret +="<div class='col s4 right-align'>Erro no Envio:</div>" +
                        "<div class='col s8'>"+leitura.erro_lancamento+"</div>";
                }
                ret +="<div class='col s12 right-align'><br><br>" +
                    "<a href='javascript: $(\"#sync_itens\").hide();' class='btn-flat waves-effect'>Fechar</a>";
                if(leitura.lancado === 'f') {
                    ret += "<a href='javascript: confirmar_exclusao(\"" + tipo + "\", \"" + valor + "\");' class='btn-flat waves-effect waves-red red-text'>Excluir</a>";
                }
                ret +="</div>" +
                    "</div>";
                $("#sync_itens .content").html(ret);
                $("#sync_itens").show();
            });
    }
}

function confirmar_exclusao(tipo, valor, callback, callback_param){
    confirmar("Deseja realmente excluir?", ()=>{deletar_registro(tipo, valor, callback, callback_param)});
}

function deletar_registro(tipo, valor, callback_function, callback_param, with_confirm=true){
    db.executeSql("DELETE FROM "+tipo+" WHERE id = "+valor, [], function(dlt){
        if(dlt.rowsAffected === 1){
            // if(tipo === 'patrimonio_inventarios_leituras'){
            //     $("#sync_itens").hide();
            //     getItens(inventario_global);
            // }
            if(with_confirm) $("#confirm").modal('close');
            window[callback_function](callback_param);
        }
    });
}

function preparaAPI(funcao, dados){
    var especifico = '';
    var queryes = '';
    if(typeof dados === "undefined" || dados.length == 0){
        var dados = [];
    }else if(typeof dados === 'number'){
        especifico = dados+'/';
    }else if(typeof dados !== 'object'){
        queryes = '&'+dados;
    }
    var d = [];
    d['data'] =         dados;
    d['type'] =         "GET";
    d['url'] =          url_athenas+funcao+'/'+especifico+'?limit=999999'+queryes;
    d['timeout'] =      3000;
    d['contentType'] =  "application/json; charset=utf-8";
    return d;
}

function sincronizaBanco(){
    getAutorizacoes();
    db.executeSql('SELECT id FROM patrimonio_inventarios_autorizacoes WHERE date(data_fim) <= date(?1)', [dataAmigavel(hoje, 'aaaa-mm-dd')], function(autorizacoes) {
        for(var i=0; i<autorizacoes.rows.length; i++){
            autorizacao = autorizacoes.rows.item(i);
            getInventarios(autorizacao.id, false);
        }
    }, function(error){
        console.log('SELECT SQL ERROR: ' + error.message);
        toast("Ocorreu um erro ao sincronizar os dados. Tente novamente mais tarde.", 'erro', 'long');
    });
    if(base_offline === true) {
        getItensTotal();
    }
}

function login(){
    var formData = new FormData();
    formData.append('username', $("#username").val().trim().toLowerCase().replace(' ', ''));
    formData.append('password', $("#password").val());

    var settings = {
        async: true,
        crossDomain: true,
        url: url_athenas+"api-token-auth/",
        type: "POST",
        processData: false,
        contentType: false,
        data: formData
    };

    let username = $("#username").val().trim().toLowerCase().replace(' ', '');

    $.ajax(settings).done(function (response) {
        if('token' in response){
            token_aut = response['token'];
            user_id = response['user_id'];
            if(get('permanecer_autenticado')) {
                set('token_aut', token_aut);
                set('user_id', user_id);
                set('usuario', username);
            }
            $("#mobile_sidenav_user a .name").html(username);
            goHome();
            $("#login").slideToggle();
            $("#login_erro").html('');
            $("#username").val('');
            $("#password").val('');
        }else{
            $("#login_erro").html('Usuário/Senha incorretos!');
        }
    }).fail(function(data, textStatus, jqXHR){
        $("#login_erro").html('Usuário ou senha inválidos!');
        console.log(data);
    });
}

function logout(){
    $('#login').slideUp();
    set('token_aut', undefined);
    set('user_id', undefined);
    set('usuario', undefined);
}

function help(){
    if(historico_nav[historico_nav.length-1] !== 'help'){
        $("#help").slideDown('fast');
        addHistorico('help');
    }else{
        $("#help").slideUp('fast');
        remHistorico();
    }
}

function sobre(){
    if(historico_nav[historico_nav.length-1] !== 'sobre'){
        $("#sobre").slideDown('fast');
        addHistorico('sobre');
    }else{
        $("#sobre").slideUp('fast');
        remHistorico();
    }
}

function confirmar(mensagem, callback){
    $("#confirm").load(dir + 'confirm.html', () => {
        $("#confirm__mensagem").html(mensagem);
        if(typeof callback === "function"){
            $("#confirm__botao_sim").on('click', ()=>{callback()});
        }else {
            $("#confirm__botao_sim").attr('href', 'javascript: ' + callback);
        }
        $("#confirm").modal('open');
        addHistorico('confirm');
    });
}

function abreScanner(onSuccess, onError=null){
    camera_aberta = true;
    var options = {
        types: {
            Code128: true,
            Code39: false,
            Code93: false,
            CodaBar: true,
            DataMatrix: false,
            EAN13: false,
            EAN8: false,
            ITF: true,
            QRCode: false,
            UPCA: false,
            UPCE: true,
            PDF417: false,
            Aztec: false
        },
        detectorSize: {
            width: .5,
            height: .7
        }
    };
    window.plugins.GMVBarcodeScanner.scan({}, function(error, result) {
        camera_aberta = false;
        if(!error){
            onSuccess(result);
        }else if (onError !== null){
            onError();
        }
    });
}

function formDigitarPlaqueta(callback){
    // TODO: IMPLEMENTAR FORMULÁRIO PARA DIGITAR A PLAQUETA
    callback();
}

function exibeDetalhesPatrimonio(plaqueta){
    $("#modal_generica").modal({onCloseEnd: ()=>{remHistorico()}});
    consultaPatrimonio(plaqueta, (patrimonio)=>{
        patrimonio = patrimonio[0];
        $("#modal_generica .modal-content").load(dir+'patrimonio_detalhes.html', ()=>{
            $('#patrimonio_detalhes__plaqueta').html(patrimonio['plaqueta']);
            $('#patrimonio_detalhes__especie').html(patrimonio['especie']['titulo']);
            $('#patrimonio_detalhes__data_compra').html(dataAmigavel(patrimonio['entrada']['data_compra']));
            $('#patrimonio_detalhes__valor_atual').html(monetario(patrimonio['valor_atual']));
            $('#patrimonio_detalhes__conservacao_display').html(patrimonio['conservacao_descricao']);
            $('#patrimonio_detalhes__localizacao_descricao').html(patrimonio['localizacao_descricao']);
            $('#patrimonio_detalhes__data_tombo').html(dataAmigavel(patrimonio['data_tombo']));
            $('#patrimonio_detalhes__descricao').html(patrimonio['descricao']);
            addHistorico('modal|modal_generica');
            $("#modal_generica").modal('open');
        });
    });
}
