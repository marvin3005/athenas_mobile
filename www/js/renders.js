function renderPagination(infos, metodo, metodo_params=null){
    var html = "<div id='pagination' class='row'>" +
        "<div class='col s2'>";
    if(infos.total > 10){
        if(infos.proximo !== null){
            url = infos.proximo.split('?');
            atual = url[1].split('offset=')[1]-10;
        }else{
            url = infos.anterior.split('?');
            atual = url[1].split('offset=')[1]+10;
        }
        url = url[0];
        html += "<a class='btn btn-flat btn-small' href='javascript: "+metodo+"(\""+infos.anterior+"\"";
        $.each(metodo_params, (i, param)=>{html += ', '+param;});
        html += ");'";
        if(infos.anterior === null) html += " disabled";
        html += ">" +
            "<i class='material-icons'>chevron_left</i>" +
            "</a>" +
            "</div><div class='input-field col s8'>" +
            "<select class='select_pagination'>";
        for(let i=0; i<infos.total/10; i++) {
            html += "<option value='"+metodo+"(\""+url+"?limit=10&offset="+i*10+"\"";
            $.each(metodo_params, (i, param)=>{html += ', '+param;});
            html+=");'";
            html += atual === i*10 ? ' selected' : '';
            html += ">"+(i+1)+"</option>";
        }
        html += "</select>" +
            "</div><div class='col s2'>" +
            "<a class='btn btn-flat btn-small' href='javascript: "+metodo+"(\""+infos.proximo+"\"";
        $.each(metodo_params, (i, param)=>{html += ', '+param;});
        html += ");'";
        if(infos.proximo === null) html += " disabled";
        html += ">" +
            "<i class='material-icons'>chevron_right</i>" +
            "</a>" +
            "</div>";
    }
    html += "</div>";

    return html;
}

function renderItensMovimentacao(registros, callback){
    var html = '<ul class="collection">';
    $.each(registros, (index, item)=>{
        html += '<li class="collection-item">' +
                '  <h5>'+item.plaqueta+'</h5>' +
                '  <span class="title">'+item.especie+'</span>' +
                '  <p>'+item.localizacao+'<br>' +
                '<div class="small">'+item.descricao+'</small>' +
                '  </p>' +
                '</li>';
        if(index === registros.length-1){
            html += '</ul>';
            callback(html);
        }
    });
}

function renderLotesMovimentacao(registros, callback){
    var html = '<ul class="collapsible popout">';
    $.each(registros, (index, item)=>{
        html += '<li>' +
            '<div class="collapsible-header">' +
            '  <div>';
        if(item['data_sincronizacao'] !== null) {
            html += '<i class="material-icons left">check</i> ';
        }
        html += item['destino'].split(': ')[1] + '</div>' +
            '  <div class="right-align">' +
            '    <a href="javascript: mostrarDetalhesLoteMovimentacao('+item['id']+');"' +
            ' class="btn btn-flat waves-effect"><i class="material-icons">remove_red_eye</i></a>' +
            '  </div>' +
            '</div>' +
            '<div class="collapsible-body">' +
            '  <b>Responsável:</b> ' + item['responsavel'].split(': ')[1] + '<br>';
        if(item['co_responsavel'] !== ''){
            html += '  <b>Co-Responsável:</b> ' + item['co_responsavel'].split(': ')[1];
        }
        html += '  <p class="center-align">' +
            '    <a href="javascript: mostrarDetalhesLoteMovimentacao('+item['id']+');" class="btn btn-flat waves-effect btn-small"><i class="material-icons">remove_red_eye</i></a> ';
        if(item['data_sincronizacao'] === null) {
            html += '    <a href="javascript: enviarLoteMovimentacao(' + item['id'] + ')" class="btn btn-flat waves-effect btn-small"><i class="material-icons">cloud_upload</i></a>' +
                '    <a href="javascript: ' +
                'abreScanner((r)=>{addItemLoteMovimentacao(r, ' + item['id'] + ');}, formDigitarPlaqueta((r)=>{addItemLoteMovimentacao(r, ' + item['id'] + ');}));' +
                '" class="btn btn-flat waves-effect btn-small"><i class="material-icons">camera_alt</i></a> ' +
                '    <a class="btn btn-flat waves-effect btn-small" ' +
                "href='javascript: confirmar_exclusao(\"patrimonio_movimento\", "+item['id']+", \"mostrarMovimentacoes\");'" +
                '><i class="material-icons">delete</i></a>' +
                '  </p>';
        }
        if(item['data_sincronizacao'] !== null) {
            html += '<p class="center-align"><i>Enviado em '+dataAmigavel(item['data_sincronizacao'], 'dd/mm/aaaa') +
                '<br>Criados os movimentos: '+item['movimentos_criados']+'</i></p>';
        }
        html += '  <hr>' +
            '  <h6 class="center-align">Itens Patrimoniais:</h6>' +
            '  <div id="lote_movimento_'+item['id']+'__itens">' +
            '    <p>Este lote não possui itens.</p>' +
            '  </div>' +
            '</div>' +
            '</li>';
            mostrarItensLoteMovimentacao(item['id'], 'lote_movimento_'+item['id']+'__itens');
        if(index === registros.length-1){
            html += '</ul>';
            callback(html);
        }
    })
}

function renderItensLoteMovimentacao(itens, lote, callback){
    dbSelect("SELECT id, data_sincronizacao FROM patrimonio_movimento WHERE id = "+lote, (lote)=>{
        let enviado = lote[0]['data_sincronizacao'] !== null;
        lote = lote[0]['id'];
        var html = '<ul class="striped">';
        $.each(itens, (i, item)=>{
            html += "<li class='row flex'>" +
                "<div class='col s10 waves-effect' onclick=\"exibeDetalhesPatrimonio('"+item['plaqueta']+"');\">" +
                "  <p><b>"+item['plaqueta']+"</b><br>" + item['especie']['titulo'] + "</p>" +
                "</div>" +
                "<div class='col s2 red-text valign-wrapper'>";
            if(enviado === false) {
                html += "  <a class='btn btn-flat btn-small waves-effect right'" +
                    " href='javascript: confirmar_exclusao(\"patrimonio_movimento_item\", " + item['id_item_lote'] + ", \"mostrarDetalhesLoteMovimentacao\", " + lote + ");'" +
                    "><i class='material-icons'>delete_outline</i></a>";
            }
            html += "</div>" +
                "</li>";
            if(i === itens.length-1){
                html += '</ul>';
                callback(html);
            }
        });
    });
}


function renderMovimentosManutencao(registros, callback){
    var html = '<ul class="collection">';
    $.each(registros, (index, item)=>{
        html += '<li class="collection-item">' +
            '  <h5>'+item.status+'</h5>' +
            '  <small>'+dataAmigavel(item.data, 'dd/mm/aaaa', false)+'</small>' +
            '  <p><b>Origem: </b>'+item.origem+'<br>' +
            '<b>Destino: </b>' +item.destino+'<br>' +
            '<b>Responsável: </b>' +item.responsavel+'<br>' +
            '<div class="small">'+item.descricao+'</small>' +
            '  </p>' +
            '</li>';
        if(index === registros.length-1){
            html += '</ul>';
            callback(html);
        }
    });
}