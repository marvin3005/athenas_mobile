const version_scripts = {
    'versions': ['1.0.0', '1.2.8',],
    'scripts':{
        '1.0.0': null,
        '1.2.8': "ALTER TABLE patrimonio_inventarios_itens ADD manutencao VARCHAR(1) default 'f';",
        '1.3.0': "CREATE TABLE IF NOT EXISTS `patrimonio_movimento` (`id` INTEGER PRIMARY KEY AUTOINCREMENT, `data` varchar(10) NOT NULL, `destino` INTEGER(4) NOT NULL, `responsavel` integer(6) NOT NULL, `co_responsavel` INTEGER(6) DEFAULT NULL, `data_sincronizacao` varchar(10) DEFAULT NULL); CREATE TABLE IF NOT EXISTS `patrimonio_movimento_item` (`id` INTEGER PRIMARY KEY AUTOINCREMENT, `movimento` integer(6) NOT NULL, `patrimonio` integer(6) DEFAULT NULL, `plaqueta` VARCHAR(12) NOT NULL);",
    }
};