function getAutorizacoes(mostrar=true){
    let registros = [];

    if(navigator.connection.type === Connection.NONE){
        //SEM INTERNET
        toast('Sem Internet! Impossível atualizar!');
        db.executeSql(
            "SELECT * FROM patrimonio_inventarios_autorizacoes ORDER BY data_inicio DESC, data_fim DESC",
            [],
            function(autorizacoes){
                //POSSUI REGISTROS NO BANCO LOCAL
                for(var a=0; a<autorizacoes.rows.length; a++){
                    autorizacao = autorizacoes.rows.item(a);
                    registros.push({
                        'id': autorizacao.id,
                        'data_inicio': autorizacao.data_inicio,
                        'data_fim': autorizacao.data_fim,
                        'publicacao_descricao': autorizacao.publicacao_descricao
                    });
                }
            }
        );
    }else{
        //COM INTERNET
        var settings = ajaxSettings('get', 'inventarios-autorizacoes/?limit=99999');
        $.ajax(settings).done(function (autorizacoes, jqXHR) {
            for(var a=0; a<autorizacoes['count']; a++){
                //CADASTRA AS AUTORIZAÇÕES NO BANCO DE DADOS LOCAL. SE JÁ EXISTIR, ATUALIZA.
                aut = autorizacoes['results'][a];
                sql = "INSERT OR REPLACE INTO patrimonio_inventarios_autorizacoes (id, data_inicio, data_fim, publicacao_descricao) VALUES ('"+aut['id']+"', '"+aut['data_inicio']+"', '"+aut['data_fim']+"', '"+aut['publicacao_descricao']+"') ";
                db.executeSql(sql, [],
                    function(insercao){
                        if(insercao.rowsAffected === 1){
                            console.log('AUTORIZAÇÃO '+insercao.insertId+' INSERIDA COM SUCESSO!');
                        }else{
                            console.log(insercao);
                        }
                    }, function(error){
                        console.log('ERRO AO INSERIR AUTORIZAÇÃO DE INVENTÁRIO NO BANCO:');
                        console.log(error.message);
                    });
                //MONTA O OBJETO
                registros.push({
                    'id': aut['id'],
                    'data_inicio': aut['data_inicio'],
                    'data_fim': aut['data_fim'],
                    'publicacao_descricao': aut['publicacao_descricao']
                });
            }
        }).fail( function(jqXHR, status){
            toast("Erro ao consultar os dados no Athenas.\nTente novamente em alguns instantes\nCaso o problema persistir entre em contato com a equipe de TI.", 'erro', "long")
        });
    }

    if(mostrar) montarHomeInventario(registros);
}

function montarHomeInventario(registros){
    let ret = "";
    if(registros.length === 0){
        //CASO NÃO TENHA NENHUM REGISTRO
        ret = '<p class="center">Sem registros para exibir.</p>';
        if(navigator.connection.type === Connection.NONE){
            $("#offline").show();
            return true;
        }
    }else {
        //CASO POSSUA REGISTROS
        for (let r = 0; r < registros.length; r++) {
            let registro = registros[r];
            ret += "<a href='javascript: getInventarios(" + registro['id'] + ");' class='waves-effect collection-item grey-text text-darken-4'>";
            ret += "<div class='col s1 collection-item-avatar'><i class='material-icons'>";
            if (Date.parse(registro['data_inicio']) <= hoje && Date.parse(registro['data_fim']) >= hoje) {
                ret += "play_circle_outline";
            } else {
                ret += "done_all";
            }
            ret += "</i></div><div class='col s11'>" + registro['publicacao_descricao'] + "<br><small>" + dataAmigavel(registro['data_inicio']) + " a " + dataAmigavel(registro['data_fim']) + "</small></div>";
            ret += "</a>";
        }
    }

    carregaConteudo(
        './fragments/inventario/autorizacoes.html',
        'Inventarios',
        'autorizacoes',
        {"#collection_autorizacoes": ret}
        );
    $("#botao_sync").show();
    $("#botao_scanner").hide();
}

function getInventarios(autorizacao, mostrar=true) {
    autorizacao_global = autorizacao;
    let registros = [];

    if(navigator.connection.type === Connection.NONE) {
        //SEM INTERNET
        toast('Sem Internet! Impossível atualizar!');
        db.executeSql(
            "SELECT * FROM patrimonio_inventarios WHERE autorizacao = ?1 ORDER BY status, localizacao_descricao",
            [autorizacao],
            function (inventarios) {
                if (inventarios.rows.length > 0) {
                    //POSSUI REGISTROS NO BANCO LOCAL
                    for (let a = 0; a < inventarios.rows.length; a++) {
                        let inventario = inventarios.rows.item(a);
                        registros.push({
                            'id': inventario.id,
                            'data': inventario.data,
                            'exercicio': inventario.exercicio,
                            'autorizacao': inventario.autorizacao,
                            'autorizacao_descricao': inventario.autorizacao_descricao,
                            'localizacao': inventario.localizacao,
                            'localizacao_descricao': inventario.localizacao_descricao,
                            'status': inventario.status
                        });
                    }
                    if (mostrar) carregarInventarios(registros);
                } else {
                    //NÃO POSSUI REGISTROS NO BANCO LOCAL
                    if(mostrar) {
                        montarHomeInventario([]);
                    }
                }
            }
        );
    } else {
        //COM INTERNET
        let settings = ajaxSettings('GET', 'inventarios-autorizacoes/'+autorizacao+'/?limit=99999');
        $.ajax(settings).done( function (autorizacoes, jqXHR) {
            if (typeof autorizacoes['detail'] === "undefined" && (Date.parse(autorizacoes['data_inicio']) <= hoje && Date.parse(autorizacoes['data_fim']) >= hoje)) {
                //AUTORIZAÇÃO ESTÁ EM PERÍODO ATIVO. SERÁ ARMAZENADO NA BASE DE DADOS LOCAL PARA POSSIBILITAR O PROCESSAMENTO OFFLINE
                let settings = ajaxSettings('GET', "inventarios/?autorizacao="+autorizacao+"&limit=99999");
                $.ajax(settings).done(function (inventarios, jqXHR) {
                    for (var i = 0; i < inventarios['count']; i++) {
                        inv = inventarios['results'][i];
                        //INSERE OU ATUALIZA O REGISTRO NO BANCO DE DADOS LOCAL
                        sql = "INSERT OR REPLACE INTO patrimonio_inventarios (id, data, exercicio, autorizacao, autorizacao_descricao, localizacao, localizacao_descricao, status) VALUES ('" +
                            inv['id'] + "','" +
                            inv['data'] + "','" +
                            inv['exercicio'] + "','" +
                            inv['autorizacao'] + "','" +
                            inv['autorizacao_descricao'].replace("'", "") + "','" +
                            inv['localizacao']+"','" +
                            inv['localizacao_descricao'].replace("'", "") + "','" +
                            inv['status'] + "')";
                        db.executeSql(sql, [],
                            function (insercao) {
                                if (insercao.rowsAffected === 1) {
                                    console.log('INVENTARIO ' + insercao.insertId + ' INSERIDO COM SUCESSO!');
                                } else {
                                    console.log(insercao);
                                }
                            });
                        //MONTA O OBJETO PARA EXIBIÇÃO
                        registros.push({
                            'id': inv.id,
                            'data': inv.data,
                            'exercicio': inv.exercicio,
                            'autorizacao': inv.autorizacao,
                            'autorizacao_descricao': inv.autorizacao_descricao,
                            'localizacao': inv.localizacao,
                            'localizacao_descricao': inv.localizacao_descricao,
                            'status': inv.status
                        });
                    }
                    if (mostrar) carregarInventarios(registros);

                    //DELETA DO BANCO DE DADOS LOCAL OS ITENS QUE FORAM REMOVIDOS DO ATHENAS
                    db.executeSql("SELECT * FROM patrimonio_inventarios WHERE autorizacao = ?1" [autorizacao], function(inventarios){
                        if(registros.length > inventarios.rows.length){
                            regs = [];
                            for(var r=0; r<registros.length; r++) {
                                regs.push(registros[r]['id']);
                            }
                            db.executeSql('DELETE FROM patrimonio_inventarios WHERE autorizacao = '+autorizacao+' AND id NOT IN ('+regs.join(', ')+')', [],
                                function(deleteds){
                                    if(deleteds.rowsAffected > 0){
                                        console.log(deleteds.rowsAffected+' INVENTARIOS FORAM EXCLUÍDOS DA BASE LOCAL.');
                                        console.log(deleteds);
                                    }else{
                                        console.log(deleteds);
                                    }
                                });
                        }
                    });
                });
            } else {
                //AUTORIZAÇÃO NÃO ESTÁ EM PERÍODO ATIVO. SERÃO EXIBIDOS SOMENTE LEITURA
                var form = new FormData();
                form.append("autorizacao", autorizacao);
                let settings = ajaxSettings('GET', 'inventarios/?limit=99999', form);
                $.ajax(settings).done(function (inventarios, jqXHR) {
                    for (var i = 0; i < inventarios['count']; i++) {
                        inv = inventarios['results'][i];
                        //MONTA O OBJETO PARA EXIBIÇÃO
                        registros.push({
                            'id': inv.id,
                            'data': inv.data,
                            'exercicio': inv.exercicio,
                            'autorizacao': inv.autorizacao,
                            'autorizacao_descricao': inv.autorizacao_descricao,
                            'localizacao': inv.localizacao,
                            'localizacao_descricao': inv.localizacao_descricao,
                            'status': inv.status
                        });
                    }
                    if (mostrar) carregarInventarios(registros);
                });
            }
        });
    }
}

function carregarInventarios(registros){
    $("#botao_scanner").hide();
    carregaConteudo("<img src='img/loading2.svg' class='loading-content'>");

    if(registros.length > 0) {
        db.executeSql('SELECT * FROM patrimonio_inventarios_autorizacoes WHERE id = ?1', [autorizacao_global], function (autorizacoes) {
            var autorizacao;
            if(autorizacoes.rows.length === 0){
                if(navigator.connection.type === Connection.NONE){
                    backButton();
                    $("#offline").show();
                    return true;
                }else{
                    dados = preparaAPI('inventarios-autorizacoes', autorizacao_global);
                    var settings = ajaxSettings('GET', "inventarios-autorizacoes/"+autorizacao_global+"/?limit=99999");
                    $.ajax(settings).done(function (result, jqXHR) {
                        console.log(result);
                        autorizacao = {
                            'id': result[0]['id'],
                            'data_inicio': result[0]['data_inicio'],
                            'data_fim': result[0]['data_fim'],
                            'publicacao_descricao': result[0]['publicacao_descricao']
                        }
                    });
                }
            }else{
                autorizacao = {
                    'id': autorizacoes.rows.item(0).id,
                    'data_inicio': autorizacoes.rows.item(0).data_inicio,
                    'data_fim': autorizacoes.rows.item(0).data_fim,
                    'publicacao_descricao': autorizacoes.rows.item(0).publicacao_descricao
                }
            }
            ret = "<div class='row'><div class='col s12'><h5>Inventários Cadastrados</h5><hr>" +
                "<div class='col s12'><span>" + autorizacao['publicacao_descricao'] + "</span><br>" +
                "<i class='material-icons tiny'>date_range</i>" + dataAmigavel(autorizacao['data_inicio']) + " a " + dataAmigavel(autorizacao['data_fim']) +
                "<div class='right'><i class='material-icons tiny'>folder</i> " + registros.length + " localidades.</div>" +
                "</div></div></div>";

            if(Date.parse(autorizacao['data_fim']) < hoje || Date.parse(autorizacao['data_inicio'] > hoje)){
                ret +="<div class='row'><div class='col s12 center grey-text'>Somente Leitura</div></div>";
            }

            ret +="<div class='collection'>";
            for (var i = 0; i < registros.length; i++){
                ret += "<a href='javascript: getItens(" + registros[i]['id'] + ", true, true);' class='waves-effect collection-item grey-text text-darken-4'>" +
                    "<p>" + registros[i]['localizacao_descricao'] + "<br>" +
                    "<small>" +
                    "<span class='collection-item-detail'><i class='material-icons tiny'>date_range</i>" + registros[i]['exercicio'] + "</span>" +
                    "<span class='collection-item-detail'><i class='material-icons tiny'>today</i>" + dataAmigavel(registros[i]['data']) + "</span>" +
                    "<span class='collection-item-detail'><i class='material-icons tiny'>" + (registros[i]['status'] === 1 ? 'play_circle_outline' : 'done_all') + "</i>" +
                    (registros[i]['status'] === 1 ? 'ABERTO' : 'CONCLUÍDO') +
                    "</span></small></p>" +
                    "</a>";
            }
            ret += "</div>";
            carregaConteudo(ret);
            addHistorico('autorizacoes');
            $("#botao_sync").hide();
            $("#botao_inicio").show();
            $("#botao_scanner").hide();
        });
    }else{
        addHistorico('autorizacoes');
        carregaConteudo("<p class='center'>Sem registros para exibir.</p>");
    }
}

function getItensTotal(url=url_athenas+"patrimonios/"){
    $("#loagind-bar").show();
    var settings = ajaxSettings('GET', url);
    $.ajax(settings).done(function (itens, jqXHR) {
        atual = url.split('offset=')[1];
        if(atual !== undefined){
            pcnt = (atual * 100) / itens['count'];
            $("#loading-bar-loaded").css('width', pcnt+'%');
            $("#loading-bar-percent").html(pcnt.toFixed(1)+'%');
        }
        itens['results'].forEach((it, index, array) => {
            //CADASTRA OS PATRIMONIOS NO BANCO DE DADOS LOCAL, CASO NÃO EXISTAM.
            sql = "SELECT * FROM patrimonio_patrimonio WHERE plaqueta = '" + it['plaqueta'] + "'";
            db.executeSql(sql, [], function (existe) {
                if (existe.rows.length === 0) {
                    sql = "INSERT INTO patrimonio_patrimonio (plaqueta, descricao, conservacao, localizacao_descricao, localizacao) VALUES ('" + it['plaqueta'] + "', '"+it['descricao'].substring(0, 31)+"', '" + it['conservacao'] + "', '" + it['localizacao_descricao'].replace("'", "").substring(0, 31) + "', '"+it['localizacao']+"') ";
                    db.executeSql(sql, [],
                        function (insercao) {
                            if (insercao.rowsAffected === 1) {
                                console.log('PATRIMONIO ' + insercao.insertId + ' ['+it['plaqueta']+'] INSERIDO COM SUCESSO!');
                            } else {
                                console.log(insercao);
                            }
                        }, function (error) {
                            console.log('ERRO AO INSERIR PATRIMONIO NO BANCO:');
                            console.log(error.message);
                        });
                }
            });
        });
        if(itens['next'] !== null){
            getItensTotal(itens['next']);
        }else{
            $("#loagind-bar").hide();
            $("#loading-bar-loaded").css('width', '0');
            $("#loading-bar-percent").html('0%');
            toast("A lista de bens patrimoniais foi atualizada.", 'sucesso', "long")
        }
    }).fail(function (jqXHR, status) {
        $("#loagind-bar").hide();
        $("#loading-bar-loaded").css('width', '0');
        $("#loading-bar-percent").html('0%');
        console.log(jqXHR);
        console.log(status);
        toast("Erro ao consultar os dados no Athenas.\nTente novamente em alguns instantes\nCaso o problema persistir entre em contato com a equipe de TI.", 'erro', "long")
    });
}

function rebuildItens(){
    sql = "DELETE FROM patrimonio_inventarios_itens WHERE inventario = "+inventario_global;
    db.executeSql(sql, [], function(itens){}, function () {
        toast('Erro ao redefinir o Banco de Dados.')
    });
    setTimeout(function(){getItens(inventario_global, true, true);}, 1000);
}

function getItens(inventario, mostrar=true, atualizar=null){
    if(mostrar) carregaConteudo("<img src='img/loading2.svg' class='loading-content'>");
    inventario_global = inventario;
    var registros = [];

    if(inventarios_atualizados.indexOf(inventario) !== -1 && atualizar !== true){
        atualizar = false;
    }

    if (navigator.connection.type === Connection.NONE || atualizar === false || atualiza_inventariados === false) {
        db.executeSql("SELECT COUNT(id) AS quant FROM patrimonio_inventarios_itens WHERE inventario = "+inventario_global,
            [], function(count_itens) {
                if (count_itens.rows.item(0).quant === 0) {
                    atualiza_inventariados = true;
                    getItens(inventario_global, mostrar, true);
                }
            });

        //SEM INTERNET
        console.log('NÃO ATUALIZA OS ITENS');
        // toast('Sem Internet! Impossível atualizar!');
        db.executeSql(
            "SELECT *, (SELECT COUNT(id) FROM patrimonio_inventarios_leituras WHERE plaqueta = I.plaqueta AND inventario = I.inventario) AS leituras FROM patrimonio_inventarios_itens I WHERE inventario = ?1 ORDER BY descricao",
            [inventario],
            function (itens) {
                if (itens.rows.length > 0) {
                    //POSSUI REGISTROS NO BANCO LOCAL
                    for (var a = 0; a < itens.rows.length; a++) {
                        item = itens.rows.item(a);
                        registros.push({
                            'id': item.id,
                            'patrimonio': item.patrimonio,
                            'plaqueta': item.plaqueta,
                            'descricao': item.descricao,
                            'conservacao': item.conservacao,
                            'conservacao_descricao': item.conservacao_descricao,
                            'inventario': item.inventario,
                            'status': item.status,
                            'leituras': itens.leituras,
                            'manutencao': itens.manutencao
                        });
                    }
                    console.log(registros);
                    if (mostrar) carregarItens(registros);
                } else {
                    if (atualiza_inventariados === false && navigator.connection.type !== Connection.NONE) {
                        getItens(inventario, mostrar, true);
                    }
                    //NÃO POSSUI REGISTROS NO BANCO LOCAL
                    else if (mostrar) {
                        addHistorico('inventarios');
                        ret = "<div class='row'><div class='col s12 center'>Sem registros a exibir.</div></div>";
                        carregaConteudo(ret);
                    }
                }
            }
        );
    } else {
        //COM INTERNET
        console.log('ATUALIZA OS ITENS');
        db.executeSql("SELECT data_inicio, data_fim FROM patrimonio_inventarios_autorizacoes WHERE id = " + autorizacao_global, [],
            function (autorizacao) {
                var settings = ajaxSettings('GET', "inventarios-itens/?limit=99999&inventario=" + inventario);
                $.ajax(settings).done(function (itens, jqXHR) {
                    inventarios_atualizados.push(inventario);
                    var processados = 0;
                    itens['results'].forEach((it, index, array) => {
                        //CADASTRA OS ITENS NO BANCO DE DADOS LOCAL, CASO NÃO EXISTAM.
                        if (Date.parse(autorizacao.rows.item(0).data_inicio) <= hoje && Date.parse(autorizacao.rows.item(0).data_fim) >= hoje) {
                            sql = "SELECT *, (SELECT COUNT(id) FROM patrimonio_inventarios_leituras WHERE plaqueta = I.plaqueta AND inventario = I.inventario) AS leituras FROM patrimonio_inventarios_itens I WHERE plaqueta = '" + it['plaqueta'] + "' AND inventario = '" + it['inventario'] + "'";
                            db.executeSql(sql, [], function (existe) {
                                if (existe.rows.length === 0) {
                                    manutencao = it['manutencao'] === true ? 't' : 'f';
                                    sql = "INSERT INTO patrimonio_inventarios_itens (patrimonio, plaqueta, descricao, conservacao, conservacao_descricao, inventario, status, manutencao) VALUES ('" + it['patrimonio'] + "', '" + it['plaqueta'] + "', '" + it['descricao'].replace("'", "") + "', '" + it['conservacao'] + "', '" + it['conservacao_descricao'].replace("'", "") + "', '" + it['inventario'] + "', '" + it['status'] + "', '" + manutencao + "') ";
                                    db.executeSql(sql, [],
                                        function (insercao) {
                                            if (insercao.rowsAffected === 1) {
                                                console.log('ITEM ' + insercao.insertId + ' INSERIDO COM SUCESSO!');
                                                //MONTA O OBJETO PARA EXIBIÇÃO
                                                sql = "SELECT COUNT(id) as leituras FROM patrimonio_inventarios_leituras WHERE plaqueta = '" + it['plaqueta'] + "' AND inventario = " + it['inventario'];
                                                db.executeSql(sql, [], function (leituras_item) {
                                                    registros.push({
                                                        'id': it['id'],
                                                        'patrimonio': it['patrimonio'],
                                                        'plaqueta': it['plaqueta'],
                                                        'descricao': it['descricao'],
                                                        'conservacao': it['conservacao'],
                                                        'conservacao_descricao': it['conservacao_descricao'],
                                                        'inventario': it['inventario'],
                                                        'status': it['status'],
                                                        'leituras': leituras_item.rows.item(0).leituras,
                                                        'manutencao': it['manutencao']
                                                    });
                                                    processados++;
                                                    if (processados === array.length) {
                                                        if (mostrar) carregarItens(registros);
                                                    }
                                                }, function () {
                                                    registros.push({
                                                        'id': it['id'],
                                                        'patrimonio': it['patrimonio'],
                                                        'plaqueta': it['plaqueta'],
                                                        'descricao': it['descricao'],
                                                        'conservacao': it['conservacao'],
                                                        'conservacao_descricao': it['conservacao_descricao'],
                                                        'inventario': it['inventario'],
                                                        'status': it['status'],
                                                        'leituras': 0,
                                                        'manutencao': it['manutencao']
                                                    });
                                                    processados++;
                                                    if (processados === array.length) {
                                                        if (mostrar) carregarItens(registros);
                                                    }
                                                });
                                            } else {
                                                console.log(insercao);
                                            }
                                        }, function (error) {
                                            console.log('ERRO AO INSERIR ITEM DE INVENTÁRIO NO BANCO:');
                                            console.log(error.message);
                                        });
                                } else {
                                    if (atualizar === true) {
                                        manutencao = it['manutencao'] === true ? 't' : 'f';
                                        sql = "UPDATE patrimonio_inventarios_itens SET inventario = '" + it['inventario'] + "', status = '" + it['status'] + "', manutencao = '" + manutencao + "' WHERE plaqueta = '" + it['plaqueta'] + "'";
                                        db.executeSql(sql, [], function (atualizacao) {
                                            if (atualizacao.rowsAffected === 1) {
                                                console.log('ITEM ' + it['plaqueta'] + ' ATUALIZADO COM SUCESSO!');
                                            } else {
                                                console.log(atualizacao);
                                            }
                                        }, function (error) {
                                            console.log('ERRO AO ATUALIZAR ITEM DE INVENTÁRIO NO BANCO:');
                                            console.log(error.message);
                                        });
                                    }
                                    //MONTA O OBJETO PARA EXIBIÇÃO
                                    sql = "SELECT COUNT(id) as leituras FROM patrimonio_inventarios_leituras WHERE plaqueta = '" + it['plaqueta'] + "' AND inventario = " + it['inventario'];
                                    db.executeSql(sql, [], function (leituras_item) {
                                        registros.push({
                                            'id': it['id'],
                                            'patrimonio': it['patrimonio'],
                                            'plaqueta': it['plaqueta'],
                                            'descricao': it['descricao'],
                                            'conservacao': it['conservacao'],
                                            'conservacao_descricao': it['conservacao_descricao'],
                                            'inventario': it['inventario'],
                                            'status': it['status'],
                                            'leituras': leituras_item.rows.item(0).leituras,
                                            'manutencao': it['manutencao']
                                        });
                                        processados++;
                                        if (processados === array.length) {
                                            if (mostrar) carregarItens(registros);
                                        }
                                    });
                                }
                            });
                        }
                    });
                }).fail(function (jqXHR, status) {
                    toast("Erro ao consultar os dados no Athenas.\nTente novamente em alguns instantes\nCaso o problema persistir entre em contato com a equipe de TI.", 'erro', "long")
                });
            });
    }
}

function getLeituras(){
    backbuton = 'itens';
    plaquetas_lidas = [];
    $("#enviados").html("<img src='img/loading2.svg' class='loading-content'>");

    var settings = ajaxSettings('GET', "inventarios-leituras/?limit=99999&inventario="+inventario_global);

    ctt = "<div class='col s12'>" +
        // "<div id='filter_leituras'></div><br>" +
        "<table id='tabela_leituras' " +
        "data-filtering='true'" +
        // "data-filter-container='#filter_leituras' " +
        "data-filter-position='center' " +
        "data-filter-placeholder='Procurar' " +
        "data-filter-min='3' " +
        "data-show-toggle='false' " +
        "data-expand-first='false' " +
        "data-sorting='true' >" +
        "<thead>" +
        "<th>Plaqueta</th>" +
        "<th data-type='date' data-format-string='DD/MM/YYYY'>Leitura</th>" +
        "<th data-type='date' data-format-string='DD/MM/YYYY HH:II'>Envio</th>" +
        "<th data-breakpoints='all'>Descrição</th>" +
        "<th data-breakpoints='all'>Autor</th>" +
        "<th data-breakpoints='all'>Substituir Plaqueta</th>" +
        "</thead><tbody>";
    $.ajax(settings).done(function (response) {
        if(response.count > 0) {
            response.results.forEach(function (leitura, index, array) {
                if(plaquetas_lidas.indexOf(leitura.plaqueta) === -1){
                    plaquetas_lidas.push(leitura.plaqueta);
                }
                subst_plaq = leitura.digitado === true ? 'Sim' : 'Não';
                data_sinc = leitura.data_sincronizacao.split('T');
                hora_sinc = data_sinc[1].split('.')[0];
                ctt += "<tr>" +
                    "<td>" + leitura.plaqueta + "</td>" +
                    "<td>" + dataAmigavel(leitura.data_leitura, 'dd/mm/aaaa', false) + "</td>" +
                    "<td>" + dataAmigavel(data_sinc, 'dd/mm/aaaa', false) + " " + hora_sinc + "</td>" +
                    "<td>" + leitura.patrimonio_descricao + "</td>" +
                    "<td>" + leitura.usuario_display + "</td>" +
                    "<td>" + subst_plaq + "</td>" +
                    "</tr>";
                if (index === response.count - 1) {
                    ctt += "</tbody></table></div>";
                    $("#enviados").html(ctt);
                    $("#badge_count_inventariados").html(plaquetas_lidas.length);
                    $('#tabela_leituras').footable({
                        "on": {
                            "ready.ft.table": function (e, ft) {
                                $(".footable-filtering-search .btn.btn-primary").html('<i class="material-icons">search</i>').css('box-shadow', 'none');
                                $(".footable-filtering-search .btn.btn-default.dropdown-toggle").remove();
                                $("#enviados .s12").css('background-color', 'white');
                            }
                        }
                    });
                }
            });
        }else{
            ctt += "</tbody></table></div>";
            $("#enviados").html(ctt);
            $("#badge_count_inventariados").html(plaquetas_lidas.length);
            $('#tabela_leituras').footable({
                "on": {
                    "ready.ft.table": function (e, ft) {
                        $(".footable-filtering-search .btn.btn-primary").html('<i class="material-icons">search</i>').css('box-shadow', 'none');
                        $(".footable-filtering-search .btn.btn-default.dropdown-toggle").remove();
                        $("#enviados .s12").css('background-color', 'white');
                    }
                }
            });
        }
    }).fail(function () {
        ctt += "</tbody></table></div>";
        $("#enviados").html(ctt);
        $("#badge_count_inventariados").html(plaquetas_lidas.length);
        $('#tabela_leituras').footable({
            "on": {
                "ready.ft.table": function (e, ft) {
                    $(".footable-filtering-search .btn.btn-primary").html('<i class="material-icons">search</i>').css('box-shadow', 'none');
                    $(".footable-filtering-search .btn.btn-default.dropdown-toggle").remove();
                    $("#enviados .s12").css('background-color', 'white');
                }
            }
        });
    });
}

function layoutItem(item, tipo=null){
    if(typeof item['status'] === 'undefined'){
        item['status'] = item.status;
        item['descricao'] = item.descricao;
        item['plaqueta'] = item.plaqueta;
        item['conservacao'] = item.conservacao;
        item['conservacao_descricao'] = item.conservacao_descricao;
        item['data'] = item.data;
        item['lancado'] = item.lancado;
        if(tipo === 'leitura'){
            item['id_leitura'] = item.id_leitura;
        }
    }

    if (item['status'] === 1) {
        status_icone = 'help_outline';
        status_desc = 'NÃO INVENTARIADO';
        status_bg = '#ffe082';
    } else if (item['status'] === 2) {
        status_icone = 'thumb_up';
        status_desc = 'INVENTARIADO';
        status_bg = '#c5e1a5';
    } else {
        status_icone = 'thumb_down';
        status_desc = 'PENDENTE';
        status_bg = '#ffab91';
    }

    if (item['conservacao'] === 1){
        conserv_bg = '#80deea';
    }else if (item['conservacao'] === 2){
        conserv_bg = '#c5e1a5';
    }else if (item['conservacao'] === 3){
        conserv_bg = '#ffe082';
    }else if (item['conservacao'] === 4){
        conserv_bg = '#ffab91';
    }

    if (item['leituras'] > 0){
        leituras_bg = '#c5e1a5';
        leituras_desc = 'LIDO';
        leituras_icone = 'done';
    }else{
        leituras_bg = '#ffe082';
        leituras_desc = 'NÃO LIDO';
        leituras_icone = 'highlight_off';
    }

    var ret = "<a href='" + (tipo === 'leitura' ? "javascript: abre_opcoes(\"patrimonio_inventarios_leituras\", \""+item['id_leitura']+"\");" : "#") + "'" +
        " class='collection-item grey-text text-darken-4' style='position:relative;' ";
    // if(tipo === 'leitura'){
    //     ret +="data-lp='delete' data-lp-tipo='patrimonio_inventarios_leituras' data-lp-valor='"+item['id_leitura']+"'";
    // }
    ret +="><div class='row'>" +
        //"<div class='col s1 collection-item-avatar'><i class='material-icons'>" + status_icone + "</i></div>" +
        "<div class='col s12'><p><b>"+item['plaqueta']+"</b> - " + item['descricao'] + "<br>" +
        "<p class='center collection-item-father'>" +
        "<span class='collection-item-detail' style='background-color: "+conserv_bg+"'><i class='material-icons tiny'>flag</i> " + item['conservacao_descricao'] + "</span>" +
        "<span class='collection-item-detail' style='background-color: "+status_bg+"'><i class='material-icons tiny'>" + status_icone + "</i> " +
        status_desc +
        "</span>";
    if(tipo !== 'leitura' && item['status'] !== 2) {
        ret += "<span class='collection-item-detail' style='background-color: " + leituras_bg + ";'>" +
            "<i class='material-icons tiny'>" + leituras_icone + "</i> " + leituras_desc + "</span>";
    }
    if (typeof item['data'] !== 'undefined' && item['data'].length > 0){
        ret += "<span class='collection-item-detail' style='background-color: #80cbc4;'><i class='material-icons tiny'>calendar_today</i> "+dataAmigavel(item['data'], 'dd/mm/aaaa', false)+"</span>";
    }

    if(tipo !== 'leitura' && (item['manutencao'] === 't' || item['manutencao'] === true)){
        ret += "<span class='collection-item-detail' style='background-color: orange;'><i class='material-icons tiny'>build</i> Em Manutenção</span>";
    }

    ret += "</p></p></div>" +
        "</div>";
    if (typeof item['lancado'] !== 'undefined' && item['lancado'] === 't'){
        ret += "<div style='position: absolute; right: 10px; top: calc(50% - 17px);'><i class='material-icons'>cloud_done</i></div>";
    }else if(typeof item['erro_lancamento'] !== 'undefined' && item['erro_lancamento'] !== null){
        ret += "<div style='position: absolute; right: 10px; top: calc(50% - 17px);'><i class='material-icons'>error</i></div>";
    }
    ret+="</a>";

    return ret;
}

function carregarItens(registros){
    $("#botao_scanner").hide();
    if(registros.length > 0) {
        sql = "SELECT * FROM patrimonio_inventarios WHERE id = "+inventario_global;
        db.executeSql(sql, [], function (inventarios) {
            db.executeSql("SELECT DISTINCT plaqueta FROM patrimonio_inventarios_leituras WHERE inventario = "+inventario_global, [], function(leituras){
                db.executeSql("SELECT DISTINCT L.id AS id_leitura, * FROM patrimonio_inventarios_leituras L JOIN patrimonio_inventarios_itens IT ON IT.patrimonio = L.patrimonio AND IT.inventario = "+inventario_global+" WHERE L.inventario = "+inventario_global+" ORDER BY id DESC", [], function(leituras){
                    db.executeSql("SELECT * FROM patrimonio_inventarios_itens WHERE inventario = "+inventario_global+" AND patrimonio NOT IN (SELECT patrimonio FROM patrimonio_inventarios_leituras WHERE inventario = "+inventario_global+")", [], function(nao_invent) {
                        db.executeSql("SELECT * FROM patrimonio_inventarios_itens WHERE inventario = "+inventario_global+" AND status <> 2", [], function (nao_inventariados) {
                            ret = "<div class='row'><div class='col s12'>" +
                                "<h5>Itens do Inventário</h5>" +
                                "<div class='btn_upload_leituras'><a id='btn_enviar_leituras' href='javascript: abre_sync_itens(" + leituras.rows.length + ", " + registros.length + ");' class='btn-flat waves-effect'><i class='material-icons'>cloud_upload</i></a></div>" +
                                "<div class='btn_sync_itens'><a id='btn_rebuild_itens' href='javascript: rebuildItens();' class='btn-flat waves-effect'><i class='material-icons'>cloud_download</i></a></div>" +
                                "<hr><div class='col s2 center-align'><i class='material-icons tiny'>" + (inventarios.rows.item(0).status === 1 ? 'play_circle_outline' : 'done_all') + "</i>" +
                                (inventarios.rows.item(0).status === 1 ? 'ABERTO' : 'CONCLUÍDO') + "</div>" +
                                "<div class='col s10'><span>" + inventarios.rows.item(0).localizacao_descricao + "</span>" +
                                // "<span><i class='material-icons tiny'>" + (inventarios.rows.item(0).status === 1 ? 'play_circle_outline' : 'done_all') + "</i>" +
                                // (inventarios.rows.item(0).status === 1 ? 'ABERTO' : 'CONCLUÍDO') + "</span>" +
                                // "<span><i class='material-icons tiny'>bookmark</i>" + leituras.rows.length + " Leituras</span>" +
                                // "<div class='right'><i class='material-icons tiny'>folder</i> " + registros.length + " itens.</div></div></div></div>" +
                                "</div></div></div>" +
                                //   "<div class='row'>" +
                                // "<div class='col s12 center'>" +
                                // "<a id='btn_enviar_leituras' class='waves-effect btn-small btn-flat' href='javascript: abre_sync_itens("+leituras.rows.length+", "+registros.length+");'>" +
                                // "<i class='material-icons'>sync</i> Enviar Leituras" +
                                // "</a> " +
                                // "<a id='btn_leituras_enviadas' class='waves-effect btn-small btn-flat' href='javascript: getLeituras();'>" +
                                // "<i class='material-icons'>playlist_add_check</i> Leituras Enviadas" +
                                // "</a>" +
                                // "</div>" +
                                //   "</div>" +
                                "<div class='row' id='conteudo_itens'>" +
                                "<div class='col s12' style='padding:0 !important;'>" +
                                "<ul class='tabs tabs-fixed-width'>" +
                                "   <li class='tab'><a class='active' href='#todos'>Todos <span class='new badge' data-badge-caption='' style='float: none; margin-left: 4px;'>"+registros.length+"</span></a></li>" +
                                "   <li class='tab'><a href='#lidos'>Leituras <span class='new badge' data-badge-caption='' style='float: none; margin-left: 4px;'>"+leituras.rows.length+"</span></a></li>" +
                                // "   <li class='tab'><a href='#nao_lidos'>Não Lidos</a></li>" +
                                "   <li class='tab'><a href='#enviados'>Inventariados <span class='new badge' data-badge-caption='' style='float: none; margin-left: 4px;' id='badge_count_inventariados'>?</span></a></li>" +
                                "   <li class='tab'><a href='#nao_enviados'>Não Inventariados <span class='new badge red' data-badge-caption='' style='float: none; margin-left: 4px;'>"+nao_inventariados.rows.length+"</span></a></li>" +
                                "</ul>" +
                                "</div>" +
                                "   <div id='todos'>" +
                                "   <div class='collection'>";
                            for (var r = 0; r < registros.length; r++) {
                                item = registros[r];
                                ret += layoutItem(item);
                            }
                            ret += "</div>" +
                                "   </div>" +
                                "   <div id='lidos'>" +
                                "   <div class='collection'>";
                            for (var i = 0; i < leituras.rows.length; i++) {
                                item = leituras.rows.item(i);
                                ret += layoutItem(item, 'leitura');
                            }
                            ret += "</div>" +
                                "   </div>" +
                                //     "   <div id='nao_lidos'>" +
                                //     "   <div class='collection'>";
                                // for (var i = 0; i < nao_invent.rows.length; i++) {
                                //     item = nao_invent.rows.item(i);
                                //     ret += layoutItem(item);
                                // }
                                // ret += "</div>" +
                                //     "   </div>" +
                                "   <div id='enviados'></div>" +
                                "   <div id='nao_enviados'>" +
                                "   <div class='collection'>";
                            for (var ni = 0; ni < nao_inventariados.rows.length; ni++){
                                ni_item = nao_inventariados.rows.item(ni);
                                ret += layoutItem(ni_item);
                            }
                            ret += "</div>" +
                                "   </div>" +
                                "</div>";

                            carregaConteudo(ret);
                            getLeituras();
                            addHistorico('inventatios');
                            $("#botao_sync").hide();
                            $("#botao_inicio").show();
                            $('.tabs').tabs();

                            if (inventarios.rows.item(0).status === 1) {
                                $("#botao_scanner").show();
                            }
                        });
                    });
                });
            });
        });
    }else{
        addHistorico('inventarios');
        carregaConteudo("<p class='center'>Sem registros para exibir.</p>");
    }
}

function itemScaneado(valor){
    console.log(valor);
    db.executeSql("SELECT * FROM patrimonio_inventarios_itens WHERE plaqueta LIKE '%"+valor+"%'", [], function(itens){
        console.log(itens);
        if(itens.rows.length > 0){
            console.log('retornou resultado!'+itens.rows.length);
            ret = "<table class='striped'>";
            ret +="<tr><td>Plaqueta:</td><td>"+itens.rows.item(0).plaqueta+"</td></tr>";
            ret +="<tr><td>Conservação:</td><td>"+itens.rows.item(0).conservacao_descricao+"</td></tr>";
            console.log(ret);
            db.executeSql("SELECT id FROM patrimonio_inventarios_itens WHERE inventario = ?1 AND patrimonio = ?2" [inventario_global, itens.rows.item(0).patrimonio], function(existe){
                if(existe.rows.length === 0){
                    ret += "<tr><td colspan='2'><i class='amber-text text-darken-1 material-icons tiny'>report_problem</i>";
                    ret += "O item lido não está na lista de inventário desta localização.</td></tr>";
                }
            }, function(){console.log('sucesso2');}, function(error){console.log('ERRO SQL2:'+error);});
            ret +="</table>";
            $("#modal_item_scaneado_conteudo").html(ret);
            modal_item_scaneado.open();
        }
    }, function(){
        console.log('sucesso');
    }, function(error){
        console.log('ERRO SQL: '+error);
    });
}

function novaLeitura(item=null, lido=false){
    console.log(item);
    leitura = item;
    backbuton = 'itens';
    impedido = false;
    db.executeSql(
        "SELECT id FROM patrimonio_inventarios_itens WHERE inventario = ?1",
        [inventario_global],
        function (itens) {
            db.executeSql(
                "SELECT DISTINCT plaqueta FROM patrimonio_inventarios_leituras WHERE inventario = ?1",
                [inventario_global],
                function (leituras) {
                    db.executeSql(
                        "SELECT localizacao, localizacao_descricao FROM patrimonio_inventarios WHERE id = ?1",
                        [inventario_global],
                        function (inventarios) {
                            db.executeSql("SELECT L.id, I.localizacao_descricao FROM patrimonio_inventarios_leituras L LEFT JOIN patrimonio_inventarios I ON I.id = L.inventario WHERE L.plaqueta = '"+(item !== false && item !== null ? item['plaqueta'] : '')+"' AND L.inventario <> "+inventario_global, [],
                                function(outras_leituras){
                                    if(outras_leituras.rows.length){
                                        impedido = true;
                                    }
                                    ret = "<div class='row'><div class='col s12'><h5>Ler Item</h5><hr><div class='col s12'>" +
                                        "<span>" + inventarios.rows.item(0).localizacao_descricao + "</span><br>" +
                                        "<span><i class='material-icons tiny'>folder</i> " + itens.rows.length + " itens.</span>" +
                                        "<span><i class='material-icons tiny'>check</i> " + leituras.rows.length + " lidos.</span>" +
                                        "<span><i class='material-icons tiny'>help</i> " + (itens.rows.length - leituras.rows.length) + " pendentes.</span>" +
                                        "</div></div></div>";

                                    if (item === false) {
                                        ret += "<div class='block_aviso' id='alerta_leitura_inventario_item_inexistente'>" +
                                            "   <div class='col s12'>" +
                                            "       <i class='deep-orange-text text-darken-1 material-icons tiny' style='font-size:40px;'>cancel</i>" +
                                            "       <p>Nenhum Patrimônio com esta identificação.</p>" +
                                            "   </div>" +
                                            "   <div class='col s12'>" +
                                            "       <a class='btn-flat' href='javascript: $(\"#alerta_leitura_inventario_item_inexistente\").remove();'>Ok</a>" +
                                            "   </div>" +
                                            "</div>";
                                    }else if(item !== null && item !== false){
                                        if(item['localizacao'] !== inventarios.rows.item(0).localizacao || impedido === true) {
                                            ret += "<div class='block_aviso' id='alerta_leitura_inventario_item_inexistente'>" +
                                                "   <div class='col s12'>" +
                                                "       <i class='amber-text text-darken-1 material-icons tiny' style='font-size:40px;'>report_problem</i>";
                                            if(impedido === false) {
                                                ret += "<p><b>O item lido não está na lista de inventário desta localização.</b><br>Localização Atual: " + item['localizacao_descricao'] + "<br>Caso pressione o botão 'Iventariar' será criada Movimentação do patrimônio para a localidade '" + inventarios.rows.item(0).localizacao_descricao + "'.</p>";
                                            }else{
                                                ret += "<p><b>Este item já foi lido em outro inventário.</b><br>Inventário Lido: " + outras_leituras.rows.item(0).localizacao_descricao + "<br>Caso necessite inventariá-lo na localidade atual, exclua a leitura pré-existente!</p>";
                                            }
                                            ret += "   </div>" +
                                                // "   <div class='col s12'>" +
                                                // "       <a class='btn-flat' href='javascript: $(\"#alerta_leitura_inventario_item_inexistente\").remove();'>Ok</a>" +
                                                // "   </div>" +
                                                "</div>";
                                        }
                                    }

                                    if (item === false || item === null) {
                                        conservacao = 0;
                                    } else {
                                        conservacao = item['conservacao'];
                                    }

                                    ret += "<div class='row'>" +
                                        "    <form class='col s12'><div class='col s12'>";
                                    if(item !== false && item !== null) {
                                        ret += "<input type='hidden' id='id' value='"+item['id']+"'>" +
                                            "<input type='hidden' id='patrimonio' value='"+item['patrimonio']+"'>";
                                    }
                                    ret += "    <div class='row'><div class='input-field col s10'>" +
                                        "          <input id='plaqueta' type='text' value='" + (item !== false && item !== null ? item['plaqueta'] : '') + "'" +
                                        "           onkeyup='javascript: $(\"#plaqueta_botao_pesquisar\").css(\"visibility\", \"visible\");'>" +
                                        "          <label id='plaqueta_label' for='plaqueta'>Plaqueta</label>" +
                                        "        </div>" +
                                        "        <div class='input-field col s2'>" +
                                        "           <a id='plaqueta_botao_pesquisar' href='javascript: consultaItemInventario($(\"#plaqueta\").val())' class='btn-flat waves-effect center' style='visibility: " + (item === null || item === false ? 'visible' : 'hidden') + ";'><i class='material-icons'>search</i></a>" +
                                        "        </div>" +
                                        "        <div class='input-field col s12'>" +
                                        "           <input type='text' class='' id='descricao' readonly='readonly' value='" + (item !== false && item !== null ? item['descricao'] : '') + "'>" +
                                        "           <label id='descricao_label' for='descricao'>Descrição</label>" +
                                        "        </div>" +
                                        "        <div class='col s12'>" +
                                        "          <select id='conservacao'>" +
                                        "               <option " + (conservacao === 0 ? 'selected' : '') + " disabled></option>" +
                                        "               <option value='1' " + (conservacao === 1 ? 'selected' : '') + ">NOVO</option>" +
                                        "               <option value='2' " + (conservacao === 2 ? 'selected' : '') + ">BOM</option>" +
                                        "               <option value='3' " + (conservacao === 3 ? 'selected' : '') + ">REGULAR</option>" +
                                        "               <option value='4' " + (conservacao === 4 ? 'selected' : '') + ">INSERVÍVEL</option>" +
                                        "          </select>" +
                                        "          <label for='conservacao'>Conservação</label>" +
                                        "       <div class='col s12'>" +
                                        "           <label>" +
                                        "               <input id='substituir_plaqueta' type='checkbox' "+(item !== false && item !== null && lido === false ? "checked='checked'" : '' )+"/>" +
                                        "               <span>Necessita substituição da plaqueta</span>" +
                                        "           </label>" +
                                        "       </div>" +
                                        "        </div></div>";
                                    if(impedido === false && item !== false && item !== null) {
                                        ret +=  "<div class='row'><div class='col s12 center'>" +
                                            "   <a href='javascript: inventariar();' class='btn waves-effect waves-light'><i class='material-icons left'>save</i> Salvar Leitura</a>" +
                                            "</div></div>";
                                    }
                                    ret +="    </form>" +
                                        "</div>";

                                    carregaConteudo(ret);

                                    $('select').formSelect();
                                    if (item !== false && item !== null) {
                                        $("#plaqueta_label").addClass('active');
                                        $("#descricao_label").addClass('active');
                                        M.updateTextFields();
                                    } else {
                                        $("#plaqueta_label").removeClass('active');
                                        $("#descricao_label").removeClass('active');
                                        M.updateTextFields();
                                    }
                                });
                        });
                });
        });
}

function consultaItemInventario(codigo, lido=false){
    db.executeSql("SELECT * FROM patrimonio_inventarios_itens WHERE plaqueta LIKE '%"+codigo+"'", [], function(item){
        console.log(item);
        if(item.rows.length === 0){
            if(navigator.connection.type === Connection.NONE) {
                db.executeSql("SELECT * FROM patrimonio_patrimonio WHERE plaqueta LIKE '%"+codigo+"%'", [], function(patrimonio){
                    console.log(patrimonio);
                    if(patrimonio.rows.length === 0){
                        novaLeitura(false, lido);
                    }else{
                        novaLeitura(patrimonio.rows.item(0), lido);
                    }
                });
            }else {
                console.log('retornou o item da api');
                var settings = ajaxSettings('GET', "patrimonios/?plaqueta="+codigo);

                $.ajax(settings).done(function (result, jqXHR) {
                    if (result['count'] > 0) {
                        result = result['results'];
                        patrimonio = {
                            'patrimonio': result[0]['id'],
                            'plaqueta': result[0]['plaqueta'],
                            'descricao': result[0]['especie']['titulo'],
                            'conservacao': result[0]['conservacao'],
                            'conservacao_descricao': result[0]['conservacao_descricao'],
                            'localizacao': result[0]['localizacao'],
                            'localizacao_descricao': result[0]['localizacao_descricao']
                        };
                        novaLeitura(patrimonio, lido);
                    } else {
                        novaLeitura(false, lido);
                    }
                });
            }
        }else{
            console.log('retornou o item da base local');
            db.executeSql("SELECT * FROM patrimonio_inventarios WHERE id = '"+item.rows.item(0).inventario+"'", [], function(inventarios){
                itm = {
                    'id': item.rows.item(0).id,
                    'patrimonio': item.rows.item(0).patrimonio,
                    'plaqueta': item.rows.item(0).plaqueta,
                    'descricao': item.rows.item(0).descricao,
                    'conservacao': item.rows.item(0).conservacao,
                    'conservacao_descricao': item.rows.item(0).conservacao_descricao,
                    'inventario': item.rows.item(0).inventario,
                    'status': item.rows.item(0).status,
                    'localizacao': inventarios.rows.item(0).localizacao,
                    'localizacao_descricao': inventarios.rows.item(0).localizacao_descricao
                };
                novaLeitura(itm, lido);
            });
        }
    });
}

function inventariar(){
    item = leitura;
    $("#loading").show();
    if($("#substituir_plaqueta").prop('checked') === true) {
        substitui_plaqueta = 't';
    }else{
        substitui_plaqueta = 'f';
    }
    var pass = true;
    db.executeSql("SELECT * FROM patrimonio_inventarios_leituras WHERE plaqueta = '"+item['plaqueta']+"' AND inventario <> '"+inventario_global+"'", [],
        function(ja_existe){
            if(ja_existe.rows.length > 0){
                Array(ja_existe.rows.length).fill(1).forEach((v, i, array) => {
                    if(ja_existe.rows.item(i).lancado === 't'){
                        toast('Não foi possível gravar a leitura, pois o patrimônio já foi inventariado em outra localidade!', 'erro', 'long');
                        pass = false;
                    }else {
                        db.executeSql("DELETE FROM patrimonio_inventarios_leituras WHERE id = " + ja_existe.rows.item(i).id, [], function (deletado) {
                            console.log('LEITURA DELETADA: ' + ja_existe.rows.item(i).plaqueta + ' DO INVENTARIO ' + ja_existe.rows.item(i).inventario);
                        }, function (error) {
                            console.log('ERRO DELETANDO INVENTÁRIO: ' + error.message);
                            toast('Não foi possível gravar a leitura, pois o patrimônio já foi lido em outro inventário e este não pôde ser excluído!', 'erro', 'long');
                            pass = false;
                        });
                    }
                })
            }
        });
    if(pass) {
        sql = "INSERT INTO patrimonio_inventarios_leituras (plaqueta, patrimonio, data, inventario, digitado) VALUES ('" +
            item['plaqueta'] + "', '" +
            item['patrimonio'] + "', '" +
            dataHoraAmigavel(new Date(), 'aaaa-mm-dd') + "', '" +
            inventario_global + "', '" +
            substitui_plaqueta + "'" +
            ")";
        console.log(sql);
        db.executeSql(sql, [],
            function (insercao) {
                if (insercao.rowsAffected === 1) {
                    console.log('LEITURA ' + insercao.insertId + ' GRAVADA COM SUCESSO!');
                    sql = "SELECT id FROM patrimonio_inventarios_itens WHERE plaqueta = '" + item['plaqueta'] + "' AND inventario = '" + inventario_global + "'";
                    console.log(sql);
                    db.executeSql(sql, [], function (existe) {
                        console.log(existe);
                        manutencao = item['manutencao'] === true ? 't' : 'f';
                        if (existe.rows.length === 0) {
                            sql = "INSERT INTO patrimonio_inventarios_itens (patrimonio, plaqueta, descricao, conservacao, conservacao_descricao, inventario, status, manutencao) VALUES (" +
                                "'" + item['patrimonio'] + "', " +
                                "'" + item['plaqueta'] + "', " +
                                "'" + item['descricao'] + "', " +
                                "'" + $("#conservacao").val() + "', " +
                                "'" + $("#conservacao option:selected").text() + "', " +
                                "'" + inventario_global + "', " +
                                // "'2' " +
                                "'1', " +
                                "'" + manutencao + "' " +
                                ")";
                        } else {
                            sql = "UPDATE patrimonio_inventarios_itens SET " +
                                "conservacao = '" + $("#conservacao").val() + "', " +
                                "conservacao_descricao = '" + $("#conservacao option:selected").text() + "', " +
                                // "status = '2', " +
                                "inventario = '" + inventario_global + "', " +
                                "manutencao = '" + manutencao + "'" +
                                "WHERE plaqueta = '" + item['plaqueta'] + "' AND inventario = '" + inventario_global + "'";
                        }
                        console.log(sql);
                        db.executeSql(sql, [], function (atualizacao) {
                            if (atualizacao.rowsAffected === 1) {
                                console.log('ITEM DE INVENTARIO ' + item['plaqueta'] + ' ATUALIZADO COM SUCESSO!');
                            } else {
                                console.log(atualizacao);
                                toast('Ocorreu um erro ao atualizar o status do Item de Inventário.', 'erro', 'long');
                            }
                            getItens(inventario_global);
                            $("#loading").hide();
                        });
                    });
                } else {
                    console.log(insercao);
                    $("#loading").hide();
                    toast('Erro ao gravar a leitura. Tente novamente!', 'erro', 'long');
                }
            },
            function (error) {
                console.log('ERRO AO GRAVAR LEITURA: ' + error.message);
                $("#loading").hide();
                toast('Erro ao gravar a leitura. Tente novamente!', 'erro', 'long');
            });
    }
}

function abre_sync_itens(inventariados, total_itens){
    db.executeSql("SELECT data_lancamento FROM patrimonio_inventarios_leituras WHERE inventario = '"+inventario_global+"' ORDER BY data_lancamento DESC", [],
        function(syncs){
            // if(syncs.rows.length > 0){
            //     dl = syncs.rows.item(0).data_lancamento;
            //     if(dl !== '' || dl !== null || typeof dl !== 'undefined' || dl !== 'null'){
            //         last_sync = "Última Transmissão: "+dataAmigavel(dl);
            //     }else{
            //         last_sync = "Nunca Transmitido!";
            //     }
            // }else{
            //     last_sync = "Nunca Transmitido!";
            // }
            ret = "<div class='row'>" +
                "<div class='col s12 center-align'><h5>Enviar Leituras</h5><hr></div>" +
                "<div class='col s6 right-align'>Itens:</div><div class='col s6 left-align'>"+total_itens+"</div>" +
                // "<div class='col s6 right-align'>Inventariados:</div><div class='col s6 left-align'>"+inventariados+"</div>" +
                "<div class='col s6 right-align'>Leituras a Enviar:</div><div class='col s6 left-align'>"+syncs.rows.length+"</div>" +
                // "<div class='col s12 center-align'>"+last_sync+"</div>" +
                "</div>";
            if(navigator.connection.type === Connection.NONE){
                ret += "<div class='row block_aviso'>" +
                    "<div class='col s12 center'>" +
                    "<i class='material-icons grey-text text-darken-1'>sync_disabled</i><br>" +
                    "Sem internet!<br>Impossível Sincronizar!" +
                    "</div>" +
                    "</div>" +
                    "<div class='row'>" +
                    "<div class='col s12 right-align'>" +
                    "<a href='javascript: $(\"#sync_itens\").hide();' class='btn-flat waves-effect'>Fechar</a>" +
                    "</div>" +
                    "</div>";
            }else{
                ret += "<hr><div class='row'>" +
                    "<div class='col s12 right-align'>" +
                    "<a href='javascript: $(\"#sync_itens\").hide();' class='btn-flat waves-effect'>Fechar</a>";
                // "<a href='javascript: getItens("+inventario_global+"); $(\"#sync_itens\").hide();' class='btn-flat waves-effect waves-green green-text'>Receber</a>" +
                if(syncs.rows.length > 0) {
                    ret += "<a href='javascript: enviar_leituras();' class='btn-flat waves-effect waves-green green-text'>Enviar</a>";
                }
                ret += "</div>" +
                    "</div>";
            }


            $("#sync_itens .content").html(ret);
            $("#sync_itens").show();
        });
}

function enviar_leituras(){
    $("#loading").show();
    db.executeSql("SELECT L.*, I.conservacao FROM patrimonio_inventarios_leituras L JOIN patrimonio_inventarios_itens I ON I.patrimonio = L.patrimonio WHERE L.inventario = "+inventario_global+" AND L.lancado = 'f'", [],
        function(leituras){
            console.log(leituras);
            Array(leituras.rows.length).fill(1).forEach(function (v, index, array) {
                leitura = leituras.rows.item(index);
                console.log(leitura);

                post_leitura(leitura);

                if((index + 1) === array.length){
                    $("#loading").hide();
                }
            });

            setTimeout(() => {
                if(remover_leituras === true){
                    sql = "DELETE FROM patrimonio_inventarios_leituras WHERE lancado = 't' AND inventario = "+leitura.inventario;
                    console.log(sql);
                    db.executeSql(sql, [], function(atualizacao){});
                    $("#sync_itens").hide();
                    getItens(inventario_global, true, true);
                }else{
                    $("#sync_itens").hide();
                    getItens(inventario_global, true, true);
                }
            }, 500);
        });
}

function post_leitura(leitura){
    var form = new FormData();
    form.append("plaqueta", leitura.plaqueta);
    form.append("patrimonio", leitura.patrimonio);
    form.append("data_leitura", leitura.data);
    form.append("inventario", leitura.inventario);
    form.append("digitado", leitura.digitado);
    form.append("conservacao", leitura.conservacao);

    // vs = [];
    // for(var v of form.values()){
    //     vs.push(v);
    // }
    // alert(JSON.stringify(vs));

    let settings = ajaxSettings('POST', 'inventarios-leituras/', form);

    $.ajax(settings).done(function (response) {
        console.log(response);
        if(remover_leituras === true){
            sql = "DELETE FROM patrimonio_inventarios_leituras WHERE id = "+leitura.id;
            console.log(sql);
            db.executeSql(sql, [], function(atualizacao){});
        }else{
            sql = "UPDATE patrimonio_inventarios_leituras SET lancado = 't', data_lancamento = '" + dataAmigavel(hoje, 'aaaa-mm-dd') + "' WHERE id = " + leitura.id;
            console.log(sql);
            db.executeSql(sql, [], function(atualizacao){});
        }
    }).fail(function(data){
        console.log(data);
        sql = "UPDATE patrimonio_inventarios_leituras SET lancado = 'f', erro_lancamento = '"+data.message+"' WHERE id = "+leitura.id;
        db.executeSql(sql, [],
            function(atualizacao){
                console.log('Erro ao enviar leitura do patrimonio '+leitura.plaqueta);
                alert('Erro ao enviar leitura do patrimonio '+leitura.plaqueta);
            });
    });

}