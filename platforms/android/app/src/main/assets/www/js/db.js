function banco(){
    var DB_NAME = "athenas";
    var DB_VERSION = "1.0";
    var DB_DESCRIPTION = "Athenas APP Local Database";
    var DB_SIZE = 1*1024*1024;
    var DB_TABLES = [[
        {
            "table":"patrimonio_inventarios",
             "fields": [
                {"name":"id",                   "type":"INTEGER", "size":6,     "default":null, "key":true},
                {"name":"data",                 "type":"VARCHAR", "size":10,    "default":"",   "key":false},
                {"name":"exercicio",            "type":"INTEGER", "size":4,     "default":"",   "key":false},
                {"name":"autorizacao",          "type":"INTEGER", "size":4,     "default":"",   "key":false},
                {"name":"autorizacao_descricao","type":"VARCHAR", "size":72,    "default":"",   "key":false},
                {"name":"localizacao",          "type":"INTEGER", "size":6,     "default":"",   "key":false},
                {"name":"localizacao_descricao","type":"VARCHAR", "size":72,    "default":"",   "key":false},
                {"name":"status",               "type":"INTEGER", "size":1,     "default":"",   "key":false}
            ]
        }
    ],[
        {
            "table":"patrimonio_inventarios_autorizacoes",
             "fields": [
                {"name":"id",                   "type":"INTEGER", "size":6,     "default":null, "key":true},
                {"name":"data_inicio",          "type":"VARCHAR", "size":10,    "default":"",   "key":false},
                {"name":"data_fim",             "type":"VARCHAR", "size":10,    "default":"",   "key":false},
                {"name":"publicacao_descricao", "type":"VARCHAR", "size":72,    "default":"",   "key":false}
            ]
        }
    ],[
        {
            "table":"patrimonio_inventarios_itens",
             "fields": [
                {"name":"id",                   "type":"INTEGER", "size":6,     "default":null, "key":true},
                {"name":"patrimonio",           "type":"INTEGER", "size":6,     "default":"",   "key":false},
                {"name":"plaqueta",             "type":"VARCHAR", "size":10,    "default":"",   "key":false},
                {"name":"descricao",            "type":"VARCHAR", "size":72,    "default":"",   "key":false},
                {"name":"conservacao",          "type":"INTEGER", "size":1,     "default":"",   "key":false},
                {"name":"conservacao_descricao","type":"VARCHAR", "size":32,    "default":"",   "key":false},
                {"name":"inventario",           "type":"INTEGER", "size":6,     "default":"",   "key":false},
                {"name":"status",               "type":"INTEGER", "size":1,     "default":"",   "key":false},
                {"name":"manutencao",           "type":"VARCHAR", "size":1,     "default":"f",  "key":false}
            ]
        }
    ],[
        {
            "table":"patrimonio_inventarios_leituras",
            "fields": [
                {"name":"id",                   "type":"INTEGER", "size":6,     "default":null, "key":true},
                {"name":"plaqueta",             "type":"VARCHAR", "size":12,    "default":"",   "key":false},
                {"name":"patrimonio",           "type":"INTEGER", "size":6,     "default":"",   "key":false},
                {"name":"data",                 "type":"VARCHAR", "size":19,    "default":"",   "key":false},
                {"name":"inventario",           "type":"INTEGER", "size":6,     "default":"",   "key":false},
                {"name":"lancado",              "type":"VARCHAR", "size":1,     "default":"f",  "key":false},
                {"name":"digitado",             "type":"VARCHAR", "size":1,     "default":"f",  "key":false},
                {"name":"data_lancamento",      "type":"VARCHAR", "size":10,    "default":"",   "key":false},
                {"name":"erro_lancamento",      "type":"VARCHAR", "size":32,    "default":null, "key":false}
            ]
        }
    ],[
        {
            "table":"patrimonio_patrimonio",
            "fields": [
                {"name":"id",                   "type":"INTEGER", "size":6,     "default":null, "key":true},
                {"name":"plaqueta",             "type":"VARCHAR", "size":12,    "default":"",   "key":false},
                {"name":"descricao",            "type":"VARCHAR", "size":32,    "default":"",   "key":false},
                {"name":"conservacao",          "type":"INTEGER", "size":1,     "default":"",   "key":false},
                {"name":"localizacao",          "type":"INTEGER", "size":6,     "default":"",   "key":false},
                {"name":"localizacao_descricao","type":"VARCHAR", "size":32,    "default":"",   "key":false}
            ]
        }
    ],[
        {
            "table":"patrimonio_movimento",
            "fields": [
                {"name":"id",                   "type":"INTEGER", "size":6,     "default":null, "key":true},
                {"name":"data",                 "type":"VARCHAR", "size":10,    "default":"",   "key":false},
                {"name":"destino",              "type":"VARCHAR", "size":80,    "default":"",   "key":false},
                {"name":"responsavel",          "type":"VARCHAR", "size":80,    "default":"",   "key":false},
                {"name":"co_responsavel",       "type":"VARCHAR", "size":80,    "default":"",   "key":false},
                {"name":"data_sincronizacao",   "type":"VARCHAR", "size":10,    "default":"",   "key":false},
                {"name":"movimentos_criados",   "type":"VARCHAR", "size":100,   "default":"",   "key":false}
            ]
        }
    ],[
        {
            "table":"patrimonio_movimento_item",
            "fields": [
                {"name":"id",                   "type":"INTEGER", "size":6,     "default":null, "key":true},
                {"name":"movimento",            "type":"INTEGER", "size":6,     "default":"",   "key":false},
                {"name":"patrimonio",           "type":"INTEGER", "size":6,     "default":"",   "key":false},
                {"name":"plaqueta",             "type":"VARCHAR", "size":12,    "default":"",   "key":false}
            ]
        }
    ]];
    
    
    //FUNÇÃO QUE CRIA AS TABELAS NO BANCO
    this.criaTabelas = function(){
        console.log('iniciou a criação das tabelas');
        db.transaction(function(tx) {
            tx.executeSql("CREATE TABLE IF NOT EXISTS `patrimonio_inventarios` (`id` INTEGER PRIMARY KEY AUTOINCREMENT, `data` varchar(10) NOT NULL, `exercicio` integer(4) NOT NULL, `autorizacao` integer(4) NOT NULL, `autorizacao_descricao` varchar(72) DEFAULT NULL, `localizacao` integer(4) NOT NULL, `localizacao_descricao` varchar(72) DEFAULT NULL, `status` integer(1) DEFAULT NULL)");
            tx.executeSql("CREATE TABLE IF NOT EXISTS `patrimonio_inventarios_autorizacoes` (`id` INTEGER PRIMARY KEY AUTOINCREMENT, `data_inicio` varchar(10) NOT NULL, `data_fim` varchar(10) NOT NULL, `publicacao_descricao` varchar(72) NOT NULL)");
            tx.executeSql("CREATE TABLE IF NOT EXISTS `patrimonio_inventarios_itens` (`id` INTEGER PRIMARY KEY AUTOINCREMENT, `patrimonio` integer(6) NOT NULL, `plaqueta` varchar(10) DEFAULT NULL, `descricao` varchar(72) DEFAULT NULL, `conservacao` integer(1) DEFAULT NULL, `conservacao_descricao` varchar(32) DEFAULT NULL, `inventario` integer(6) DEFAULT NULL, `status` integer(1) DEFAULT NULL, `manutencao` varchar(1) DEFAULT 'f')");
            tx.executeSql("CREATE TABLE IF NOT EXISTS `patrimonio_inventarios_leituras` (`id` INTEGER PRIMARY KEY AUTOINCREMENT, `plaqueta` varchar(12) NOT NULL, `patrimonio` integer(6) DEFAULT NULL, `data` varchar(19) DEFAULT NULL, `inventario` INTEGER DEFAULT NULL, `lancado` varchar(1) NOT NULL DEFAULT 'f', `digitado` varchar(1) NOT NULL DEFAULT 'f', `data_lancamento` varchar(10) DEFAULT NULL, `erro_lancamento` varchar(32) DEFAULT NULL)");
            tx.executeSql("CREATE TABLE IF NOT EXISTS `patrimonio_patrimonio` (`id` INTEGER PRIMARY KEY AUTOINCREMENT, `plaqueta` varchar(12) NOT NULL, `descricao` VARCHAR(32) DEFAULT NULL, `conservacao` integer(1) DEFAULT NULL, `localizacao_descricao` varchar(32) DEFAULT NULL, `localizacao` integer(4) DEFAULT NULL)");
            tx.executeSql("CREATE TABLE IF NOT EXISTS `patrimonio_movimento` (`id` INTEGER PRIMARY KEY AUTOINCREMENT, `data` varchar(10) NOT NULL, `destino` VARCHAR(80) NOT NULL, `responsavel` VARCHAR(80) NOT NULL, `co_responsavel` VARCHAR(80) DEFAULT NULL, `data_sincronizacao` varchar(10) DEFAULT NULL, `movimentos_criados` varchar(100) DEFAULT NULL)");
            tx.executeSql("CREATE TABLE IF NOT EXISTS `patrimonio_movimento_item` (`id` INTEGER PRIMARY KEY AUTOINCREMENT, `movimento` integer(6) NOT NULL, `patrimonio` integer(6) DEFAULT NULL, `plaqueta` VARCHAR(12) NOT NULL)");
        }, function(error) {
            console.log('Transaction ERROR: ' + error.message);
        }, function() {
            console.log('Populated database OK');
        });
    };

    this.insere = function(tabela, campos, valores, callback){
        if(campos.length != valores.length){
            console.log("Erro: Quantidade de Campos diferente da quantidade de Valores!")
            callback(false);
        }else{
            var sql = "INSERT INTO `"+tabela+"` ("+campos.join(", ")+") VALUES (";
            var vals = [];
            for(var i=0; i<valores.length; i++){
                vals[i] = "'"+valores[i]+"'";
            }
            sql+= vals.join(", ")+");";
            // console.log("SQL: "+sql);
        }
        db.transaction(function(tx) {
            tx.executeSql(sql, [], function(tx, rs) {
                // console.log(rs);
                callback(true);
            }, function(tx, error) {
                console.log('Error: ' + error.message);
                callback(false);
            });
        });
    }

    this.insereVarios = function(sql, callback){
        db.transaction(function(tx) {
            tx.executeSql(sql, [], function(tx, rs) {
                //console.log(rs);
                callback(true);
            }, function(tx, error) {
                console.log('Error: ' + error.message);
                callback(false);
            });
        });
    }

    this.query = function(sql, callback){
        // console.log("QUERY SQL:\n\t"+sql);
        db.transaction(function(transaction) {
            transaction.executeSql(sql, [], function (tx, results) {
                var len = results.rows.length;
                var registers = [];
                for (var i = 0; i < len; i++) {
                    registers[i] = results.rows.item(i);
                }
                var res;
                res = {
                    'tamanho': len,
                    'linha': registers
                };
                //console.log(res);
                var r = JSON.stringify(res);
                //console.log(r);
                callback(r);
            },  function(tx, error) {
                console.log('Error: ' + error.message);
                callback(false);
            });
        });
    }


    this.seleciona = function(tabela, where, callback){
        var sql = "SELECT * FROM "+tabela+" WHERE "+where;
        this.query(sql, function(data){callback(data);});
    }

    this.selecionaTodos = function(tabela, callback){
        var sql = "SELECT * FROM "+tabela;
        this.query(sql, function(data){callback(data);});
    }
    
    
}